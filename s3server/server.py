from s3.layout import LayoutReader
from s3.protocol import Message
from s3.reader import Reader
from s3.signal import Signal, SignalState
from .download import DownloadServer
from .session import Session, SessionMode
import asyncio
import socket
import zlib

class ServerError(Exception):
	pass

class Server:

	def __init__(self, name, intg_file, layout_file, login_provider,
			host=None, port=1954, ssl=None, warn=False):
		self.name = name

		self.host = host
		self.port = port
		self.server = None
		self.loop = None

		self.clients = {}
		self.login_provider = login_provider
		self._ssl = ssl

		self.network = None
		with open(intg_file) as f:
			self.data = data = f.read()
			self.compressed_data = zlib.compress(data.encode("utf-8"))
			reader = Reader(data, intg_file, warn=warn)
			self.network = reader.create_network()

		if self.network is None:
			raise ServerError("no network to control")

		self.layouts = None
		with open(layout_file) as f:
			self.layout_data = data = f.read()
			self.compressed_layout_data = zlib.compress(data.encode("utf-8"))
			reader = LayoutReader(self.network, data, layout_file, warn=warn)
			self.layouts = reader.create_layouts()

		if self.layouts is None:
			raise ServerError("invalid layout")

		for intg in self.network.interlockings:
			for lever in intg.levers:
				lever.on_change_state(self._on_change_lever_state)

		for switch in self.network.switches:
			switch.on_change_state(self._on_change_switch_state)

		for signal in self.network.signals:
			signal.on_change_state(self._on_change_signal_state)

		for track in self.network.tracks:
			for sec in track.sections:
				sec.on_change_occupied(self._on_change_track_occupied)

		for signal in self.network.signals:
			signal.reset()

	async def start(self):
		try:
			self.loop = asyncio.get_event_loop()
			self.server = await asyncio.start_server(
				self.on_client_connect, ssl=self._ssl,
				host=self.host, port=self.port)
		except Exception as e:
			raise ServerError(e.args[1])

	def stop(self, user):
		if self.loop is None:
			return

		for token in list(self.clients.keys()):
			clients = self.clients[token][:]
			for session in clients:
				session.send(Message("099", "Server shutdown", prefix=user))
				self.close(session)

		self.loop.call_soon_threadsafe(self.loop.stop)

	def close(self, sess):
		if sess.token in self.clients:
			self.clients[sess.token].remove(sess)
			if len(self.clients[sess.token]) == 0:
				del self.clients[sess.token]
		sess.close()

	def broadcast(self, level, msg):
		for token in self.clients.keys():
			for sess in self.clients[token]:
				if sess.level >= level:
					sess.send(msg)

	async def send_data(self, sess):
		sess.dlserver1 = dlserver1 = DownloadServer(self.compressed_data)
		sess.dlserver2 = dlserver2 = DownloadServer(self.compressed_layout_data)
		await asyncio.gather(dlserver1.start(), dlserver2.start())
		sess.send(Message("010", dlserver1.port, dlserver2.port))

	async def send_system_state(self, sess):
		for intg in self.network.interlockings:
			for lever in intg.levers:
				if not lever.normal:
					state = "R" if lever.reverse else "X"
					sess.send(Message("LEVER", state, lever))

		for switch in self.network.switches:
			if not switch.normal:
				state = "R" if switch.reverse else "X"
				sess.send(Message("SWITCH", state, switch))

		for signal in self.network.signals:
			if not signal.not_clear:
				state = "R" if signal.clear else "X"
				sess.send(Message("SIGNAL", state, signal))

		for track in self.network.tracks:
			for sec in track.sections:
				if sec.occupied:
					offset = (sec.up + sec.down) / 2
					sess.send(Message("TRACK", "R", sec))

	@staticmethod
	def address(writer):
		return "{0}:{1}".format(*writer.get_extra_info("peername"))
	
	async def on_client_connect(self, reader, writer):
		addr = Server.address(writer)
		sess = Session(self, reader, writer)
		sess.send(Message("001", self.name))

		while True:
			line = (await reader.readline()).decode("utf-8").strip()
			if line == "":
				break

			msg = Message.from_line(line)
			cmd = msg.command.upper()

			if cmd == "LOGIN":
				if sess.token is not None:
					sess.send(Message("093", "Already logged in"))
					continue
				if sess.mode is not None:
					sess.send(Message("094", "Must send LOGIN before MODE"))
					break
				data = self.login_provider.authenticate(msg.params)
				if "error" in data:
					sess.send(Message("091", data["error"]))
					break
				sess.token = data["token"]
				sess.level = data["level"]

				if sess.level < 25:
					sess.send(Message("092", "Insufficient permissions"))
					break

				if sess.token not in self.clients:
					self.clients[sess.token] = []
				self.clients[sess.token].append(sess)

				sess.send(Message("002", sess.token))
			elif cmd == "MODE":
				if sess.mode is not None:
					sess.send(Message("090", "Session mode already set"))
					continue
				if sess.token is None:
					sess.send(Message("094", "Must send LOGIN before MODE"))
					break
				if len(msg.params) != 1:
					sess.send(Message("095", "Wrong number of parameters"))
				mode = msg.params[0].upper()
				if mode == "NX":
					sess.mode = SessionMode.ENTRANCE_EXIT
				elif mode == "LEVER":
					sess.mode = SessionMode.LEVER
				elif mode == "OBSERVE":
					sess.mode = SessionMode.OBSERVE
				elif mode == "OPER":
					sess.mode = SessionMode.OPERATOR
				else:
					sess.send(Message("096", "Invalid parameter to MODE"))
					continue	
				sess.send(Message("003", mode, "Session mode set"))
				await self.send_data(sess)
			else:
				if sess.token is None and sess.mode is None:
					sess.send(Message("097", "Must be logged in"))
				ret = await sess.handle(msg)
				if ret == 0:
					if sess.dlserver1 is not None:
						sess.dlserver1.close()
					if sess.dlserver2 is not None:
						sess.dlserver2.close()
					return

		self.close(sess)

	async def switch(self, sess, lever):
		lvr = self.network.lever(lever)

		if lvr is None:
			sess.send(Message("101", lever, "Lever does not exist"))
			return
		if not lvr.can_switch():
			sess.send(Message("102", lvr, "Lever locked out"))
			return

		self.broadcast(50, Message("CALL", lever, prefix=sess.token))
		res = await lvr.switch()

	async def track_switch(self, sess, track):
		trk = self.network.track_section(track)

		if trk is None:
			sess.send(Message("110", track, "Track section does not exist"))
			return

		self.broadcast(10, Message("TOGGLE", track, prefix=sess.token))
		trk.occupied = not trk.occupied

	async def find_routes(self, sess, lever):
		obj = self.network.lever_object(lever)
		if not isinstance(obj, Signal):
			return

		lvr = obj.lever
		return lvr.interlocking.routes_from(lvr)

	def _on_change_lever_state(self, lever):
		state = "N" if lever.normal else "R" if lever.reverse else "X"
		self.broadcast(50, Message("LEVER", state, lever))

		for obj in lever.controls.values():
			if isinstance(obj, Signal):
				self._on_change_signal_state(obj)

	def _on_change_switch_state(self, switch):
		state = "N" if switch.normal else "R" if switch.reverse else "X"
		self.broadcast(10, Message("SWITCH", state, switch))

	def _on_change_signal_state(self, signal):
		state = "N" if signal.not_clear else "R" if signal.clear else "X"
		if signal.lever is not None and signal.lever.normal:
			state = "N"
		self.broadcast(10, Message("SIGNAL", state, signal))

	def _on_change_track_occupied(self, track):
		state = "R" if track.occupied else "N"
		self.broadcast(10, Message("TRACK", state, track))
