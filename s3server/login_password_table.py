from crypt import crypt
from .login import LoginProvider

class PasswordTableLoginProvider(LoginProvider):
	"""
	A login provider that authenticates
	based on a password table file.
	"""

	def __init__(self, filename):
		self.table = {}
		with open(filename) as f:
			self._populate_table(f)

	def _populate_table(self, file):
		for line in file:
			# line format: username:access_level:password
			username, access_level, password = line.strip().split(":")
			self.table[username] = (min(int(access_level), 255), password)

	def authenticate(self, credentials):
		username, password = credentials
		if username not in self.table:
			return {"error": "No such user"}

		access_level, user_hash = self.table[username]
		if crypt(password, user_hash) != user_hash:
			return {"error": "Invalid password"}

		return {"token": username, "level": access_level}
