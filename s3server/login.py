class LoginProvider:
	"""
	A class for implementing login providers. A
	login provider authenticates an S3 user and
	returns a token which uniquely identifies
	them to the server and also indicates their
	permission level.
	"""

	def __init__(self):
		pass

	def authenticate(self, credentials):
		"""
		Given login credentials, attempt to authenticate
		this user's identity. If successful, return a
		dict-like object containing the user's identity
		token and permission level.
		"""

		raise NotImplementedError
