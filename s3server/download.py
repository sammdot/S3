import asyncio
import socket

class DownloadServerError(Exception):
	pass

class DownloadServer:

	def __init__(self, data):
		self.data = data
		self.v4server = None
		self.v6server = None

	async def start(self):
		try:
			self.v4server = await asyncio.start_server(
				self.on_connect, host=None, port=0, family=socket.AF_INET)
			self.v6server = await asyncio.start_server(
				self.on_connect, host=None, port=self.port, family=socket.AF_INET6)
		except Exception as e:
			raise DownloadServerError(e.args[1])

	@property
	def port(self):
		return self.v4server.sockets[0].getsockname()[1]

	def on_connect(self, reader, writer):
		writer.write(self.data)
		writer.close()

	def close(self):
		self.v4server.close()
		self.v6server.close()
