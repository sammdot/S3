from enum import Enum
from operator import itemgetter
from s3.protocol import Message

class SessionMode(Enum):
	"""
	Different modes of operation in a single
	session. Lever mode and NX mode are supported.
	"""
	LEVER = 1
	ENTRANCE_EXIT = 2
	OBSERVE = 3
	OPERATOR = 4

class Session:

	def __init__(self, server, reader, writer):
		self.mode = None
		self.server = server
		self.network = server.network

		self.token = None
		self.level = None

		self.reader = reader
		self.writer = writer

		self.dlserver1 = None
		self.dlserver2 = None

		self.commands = {}

		self._routes = None
		self._exit_signals = None

	def send(self, message):
		self.writer.write("{0}\n".format(str(message)).encode("utf-8"))

	def close(self):
		if self.dlserver1 is not None:
			self.dlserver1.close()
		if self.dlserver2 is not None:
			self.dlserver2.close()
		self.writer.close()

	async def handle(self, msg):
		if self.token is None:
			return

		cmd = msg.command.upper()

		if cmd == "LOGOUT":
			self.send(Message("098", "Goodbye"))
			self.server.close(self)
			return 0
		elif cmd == "KILL":
			if self.level < 255:
				self.send(Message("092", "Insufficient permissions"))
			else:
				self.server.stop(self.token)
		elif cmd == "DLACK":
			self.dlserver1.close()
			self.dlserver2.close()
			self.dlsserver1 = None
			self.dlsserver2 = None
			self.send(Message("011", "Download acknowledged"))
			await self.server.send_system_state(self)

		elif cmd == "TOGGLE":
			if self.mode != SessionMode.OPERATOR:
				self.send(Message("111", "Only available in operator mode"))
			elif self.level < 10:
				self.send(Message("092", "Insufficient permissions"))
			else:
				await self.server.track_switch(self, msg.params[0])

		elif cmd == "CALL":
			if self.mode != SessionMode.LEVER and self.mode != SessionMode.ENTRANCE_EXIT:
				self.send(Message("100", "Only available in lever or NX mode"))
			elif self.level < 50:
				self.send(Message("092", "Insufficient permissions"))
			else:
				await self.server.switch(self, msg.params[0])

		elif cmd == "ENTER":
			if self.mode != SessionMode.ENTRANCE_EXIT:
				self.send(Message("120", "Only available in NX mode"))
			elif self.level < 50:
				self.send(Message("092", "Insufficient permissions"))
			else:
				self._routes = await self.server.find_routes(self, msg.params[0])
				if self._routes:
					self._exit_signals = sorted(set(map(itemgetter(1), self._routes)), key=str)
					for signal in self._exit_signals:
						self.send(Message("EXIT", signal.lever if signal.lever else signal))
					self.send(Message("121", "End of exit options"))
				else:
					self.send(Message("090", "No routes found"))
