S3 (Subway Signal System)
=========================

|Build Status|

A control system for virtual railroad interlockings, based on the NX/UR
system used on the Toronto and New York City subways.

S3 itself is a signal *server*; it takes an S3 track network description
file (``.s3``) containing the layout of the system being controlled,
including tracks, switches, signals, control lengths and interlocking
logic, and operates the interlocking based on the positions of trains on
the system.

S3 can be operated in two modes, *entrance-exit* mode and *lever* mode.
Entrance-exit (NX) mode allows the tower operator to select start and
end points for a route across an interlocking, that the system will then
calculate automatically; lever mode allows the operator to manually
operate levers to control signals and switches in the interlocking, as
in older mechanical interlockings.

.. |Build Status| image:: https://travis-ci.org/sammdot/S3.svg?branch=dev
   :target: https://travis-ci.org/sammdot/S3
