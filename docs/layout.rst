S3 Display Language (``.s3l``)
==============================

This file describes the format for the language used to describe the
*display* of track networks in S3. The interlocking language described
in `language.md <./language.md>`__ is used to describe the *layout* of
the network, including tracks, switches, signals, and interlocking
logic; display language describes how the network is displayed by the
client.

As with interlocking language, ``#`` designates the start of a comment,
and ``network`` must be the first statement.

Coordinates
-----------

The coordinate plane of the client's display panel has the *top-left*
corner of the panel as its origin (0, 0). All position units used below
are relative to this origin. Each unit may have a different size on the
screen, depending on the viewing mode; for example, the scale is smaller
on an overview map than on an interlocking view.

All numbers must be positive.

``network``
-----------

The ``network`` statement gives the name of the track network being
described in the rest of the file. This must match the name of the track
network in the corresponding interlocking language file.

::

    network <name>

``viewport``
------------

The ``viewport`` statement defines the dimensions of the display. The
viewport is then defined as the rectangle with the top-left corner
(``x1``, ``y1``) and the bottom-right (``x2``, ``y2``). Only objects
within the viewport are displayed by the client; anything else is
clipped. This statement must appear immediately after ``network``.

::

    viewport <x1> <y1> <x2> <y2>

Block statements
----------------

The block statement describes the shape of a specific track section.
``path`` is the SVG path data that describes how this section is to be
drawn on the display.

::

    <track>@<offset> <path>

All signal objects are drawn relative to the track sections.

Why not draw the entire track?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Drawing entire tracks results in geographically accurate scale, which
*might not be a great idea* when a larger amount of information is being
displayed and irrelevant objects, e.g. sections with automatic signals,
take the same priority on the display as more important ones, like
switches.

Switch statements
-----------------

The switch statement describes the position of a switch on a block.
Since the block will have already been positioned, this statement only
provides the relative position of the switch on the block, in terms of
the length of the block, where ``0`` and ``1`` are the start and end of
the path as described in the block statement.

``direction`` is the direction to which the switch branches out from the
main track, relative to *up*. Valid values are ``left`` and ``right``.

::

    <interlocking>/<lever> <direction> <offset>

Interlocking statements
-----------------------

The interlocking statement defines the viewport for an interlocking,
which is a rectangle on the display that contains all objects in the
interlocking (signals and switches).

::

    <interlocking> <x1> <y1> <x2> <y2>

The viewport of the specified interlocking is defined as the rectangle
with the top-left corner at (``x1``, ``y1``) and the bottom-right corner
at (``x2``, ``y2``).
