S3 Server Protocol
==================

This file describes the protocol used to communicate between an S3
server and an S3 client.

Connection
----------

Connections can only be established via TCP; servers listen on port 1954
by default, but this may be changed by the server configuration. SSL/TLS
encryption may or may not be used on the server; it is left to the
client to know whether to use SSL. SSL connections are preferred, since
login credentials may be sent over plain text.

Each active connection between the S3 server and a client is stored in a
*session* object. This object contains other information about the
connection, such as the user who initiated the connecion, and their
permission level on the server. Multiple sessions are allowed for the
same client.

Permission Levels
-----------------

The S3 server uses a permissions system to manage what a user can or
cannot do on the server. Permission levels are between 0 and 255; any
value outside this range will not be accepted.

Below are the *minimum* permission levels for the specified features:

-  0 - The user is not allowed to login to the server.

-  10 - The user may set occupancy of tracks. This level is intended for
   train operators who are not allowed to observe the control system.

-  25 - The user may *observe* the state of the system on the server.

-  50 - The user may switch levers in lever mode, and set routes in NX
   mode.

-  100 - The user may set or unset manual control on any signals.

-  255 - The administrator of the server. May shut down the server.

Message Format
--------------

S3 protocol messages are formatted in the same way as IRC. `RFC
2812 <http://tools.ietf.org/html/rfc2812>`__ describes this format in
detail. Messages must be encoded in UTF-8.

An S3 protocol message looks like the following:

::

    COMMAND param param ... :param

::

    :prefix COMMAND param param ... :param

A message generally consists of a command (or numeric status code),
followed by up to 15 parameters, the last one of which may include
spaces; some messages may include a prefix. Messages must end in CR LF
and may not be more than 512 bytes long (including the CRLF).

Commands
--------

``LOGIN``
~~~~~~~~~

::

    LOGIN credentials...

Login to the S3 server using the specified credentials. The number and
order of parameters to this command may vary depending on the login
provider used for the server. (The default login provider uses a
password table, which requires a username and a password.)

This command must be sent first. If login is successful, a ``002``
message should be sent.

``MODE``
~~~~~~~~

::

    MODE mode

Set the control mode for the current control session. There are four
possible values for ``mode``:

-  ``LEVER``: lever mode involves controlling individual levers in an
   interlocking;

-  ``NX``: entrance-exit mode involves selecting entrance and exit
   points for a route through an interlocking and having the system
   automatically calculate the best route.

-  ``OBSERVE``: observer mode only lets the user see the state of the
   system but not make any changes to it.

-  ``OPER``: operator mode lets the user change the occupancy of tracks,
   as if they were operating a train moving through the track network.

If successful, a ``003`` message and a ``010`` message should be sent.

``DLACK``
~~~~~~~~~

::

    DLACK

Acknowledge receipt of the interlocking data sent by the server. When
this command is received by the server, the ports for the download
server may be closed. A ``011`` message is sent in response.

``ENTER``
~~~~~~~~~

::

    ENTER interlocking/lever

Set a new route starting at the signal controlled by the numbered
``lever`` in the named ``interlocking``. (Note that the command requires
the *lever number*, and not the *signal*, as its parameter.) The server
then calculates all the possible routes across the interlocking,
starting from the signal, and sends ``EXIT`` messages back to the client
to select the other end of the route.

If no such routes are found, a ``090`` message is returned.

This command is only available in NX mode.

``EXIT``
~~~~~~~~

::

    EXIT interlocking/lever

::

    EXIT signal

An option for an exit signal for the current route, which can be either
a home signal (in which case the ``interlocking/lever`` syntax is used)
or an automatic signal (for which the ``signal`` syntax is used).

If sent by the server, indicates a possible option for an exit signal
to end a route. This is only sent after an ``ENTER``. The client should
wait until a ``121`` message is sent before selecting an exit signal.

If sent by the client, indicates that a route ending at the specified
signal is desired. This should only be sent after a ``121`` message.
If successful, the server will set the route and clear the appropriate
switches and signals.

``CALL``
~~~~~~~~

::

    CALL interlocking/lever

Move the numbered ``lever`` in the named ``interlocking`` to the
opposite position from what it currently is (i.e. ``N`` to ``R`` or
``R`` to ``N``). If the lever cannot switch because the interlocking
prevents it, a ``102`` is sent. This command is only available in lever
and NX mode.

::

    :user CALL interlocking/lever

Broadcasted by the server. ``user`` is the unique identifier of the user
who switched the lever.

``LEVER``
~~~~~~~~~

::

    LEVER state interlocking/lever

Broadcasted by the server. The state of the ``lever`` in the
``interlocking`` has been changed to ``state``, which can be ``N``,
``R`` or ``X`` (``X`` is the state between normal and reverse).

``SWITCH``
~~~~~~~~~~

::

    SWITCH state interlocking/lever

Broadcasted by the server. The state of the switch controlled by the
lever has been changed to ``state``, which can be ``N``, ``R`` or ``X``.

``SIGNAL``
~~~~~~~~~~

::

    SIGNAL state :signal number

Broadcasted by the server. The state of the signal with the specified
signal number has been changed to ``state``, which can be ``N``, ``R``
or ``X``, with the same meanings as above (``X`` is generally for timed
signals which are about to clear).

``TRACK``
~~~~~~~~~

::

    TRACK state track@offset

Broadcasted by the server. The state of the track section that contains
the specified offset on the track has been changed to ``state``, which
can be ``N`` or ``R``, where ``R`` is used when the track section is
occupied.

``TOGGLE``
~~~~~~~~~~

::

    TOGGLE track@offset

Toggle the track section that contains the specified offset on the track
to occupied or unoccupied, depending on the state it was already at.
This command is only available in operator mode.

::

    :user TOGGLE track@offset

Broadcasted by the server. The occupancy of the track section that
contains the specified offset on the track has been toggled.

``LOGOUT``
~~~~~~~~~~

::

    LOGOUT

Logout of the S3 server. A ``098`` message is sent back to the client,
then the connection is cut. This will send an error if there is no user
logged in.

``KILL``
~~~~~~~~

::

    KILL

Stop the S3 server. This command is for administrator use only and
requires a permission level of 255. If executed, a ``099`` message is
sent to all connected clients and the sessions are closed.

Numeric Status Codes
--------------------

``001``
~~~~~~~

::

    001 server_name

This message is sent to the client once a successful connection is
initiated. The server provides the client with its name (which may or
may not be the same as its domain name). At this point, the client may
start logging in.

``002``
~~~~~~~

::

    002 ident

Once the client successfully logs in, the server sends the user's unique
identifier (``ident``), which it uses to keep track of all this user's
connections.

``003``
~~~~~~~

::

    003 mode :Session mode set

The control mode for the session has been set to ``mode`` (``NX``,
``LEVER``, ``OBSERVE``, ``OPER``).

``010``
~~~~~~~

::

    010 port1 port2

After logging in, the server sends the interlocking data to the client.
The data is encoded using ``zlib``, and ports ``port1`` and ``port2``
are opened on the server to listen for a client connection; the client
may connect to the ports to download the data, and send a ``DLACK`` to
let the server know the data has been downloaded.

``port1`` sends the data for interlocking logic; ``port2`` sends the
graphical layout data.

``011``
~~~~~~~

::

    011 :Download acknowledged

The download of the interlocking data has been acknowledged, and the
server has closed the download server.

``090``
~~~~~~~

::

    090 :No routes found

Sent in response to an ``ENTER`` message, when no routes can be found
originating from the specified lever. The client should not attempt this
lever again.

``091``
~~~~~~~

::

    091 :message

The login provider may return an error message ``message`` on an
unsuccessful authentication for any reason, for example, invalid login
credentials or a nonexistent user.

``092``
~~~~~~~

::

    092 :Insufficient permissions

Sent when the last command received requires a higher permission level
than the user currently has. The client should not attempt to send the
command again.

``093``
~~~~~~~

::

    093 :Already logged in

The user has already previously sent a ``LOGIN`` message and should not
attempt to do it again.

``094``
~~~~~~~

::

    094 :Must send MODE before LOGIN

A ``LOGIN`` message was sent before a ``MODE`` message; ``MODE``
messages must always be sent first.

``095``
~~~~~~~

::

    095 :Wrong number of parameters

The last command had the wrong number of parameters.

``096``
~~~~~~~

::

    096 :Invalid parameter to MODE

The ``MODE`` command was given an unrecognized or invalid parameter. See
the documentation on the ``MODE`` command for the meanings of the valid
values.

``097``
~~~~~~~

::

    097 :Must be logged in

The last command requires that the user be logged in. This applies to
every command except ``LOGIN`` and ``MODE``. The user should not attempt
to send the command again without logging in.

``098``
~~~~~~~

::

    098 Goodbye

Sent when the client logs out of the server.

``099``
~~~~~~~

::

    :user 099 :Server Shutdown

Sent when the server is being shut down. ``user`` is the unique
identifier of the user who initiated the shutdown.

``100``
~~~~~~~

::

    100 :Only available in lever mode

The last command is only available in lever mode.

``101``
~~~~~~~

::

    101 interlocking/lever :Lever does not exist

There is no lever by that number in the interlocking, or there is no
interlocking by that name.

``102``
~~~~~~~

::

    102 lever :Lever locked out

The lever cannot switch due to physical restrictions of the interlocking
that would otherwise create a possibly unsafe movement.

``110``
~~~~~~~

::

    110 track@offset :Track section does not exist

There is no track section with that offset on the specified track.

``111``
~~~~~~~

::

    111 :Only available in operator mode

The last command is only available in operator mode.

``120``
~~~~~~~

::

    120 :Only available in NX mode

The last command is only available in NX mode.

``121``
~~~~~~~

::

    121 :End of exit options

Sent after an ``EXIT``, denoting that the list of options for route
exits has been exhausted and the client may send an ``EXIT`` reply.
