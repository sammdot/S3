S3 Interlocking Language (``.s3``)
==================================

S3 interlocking language can be used to define a track network for S3,
including tracks, switches, signals and interlocking logic. Each
interlocking language file contains information about one track network,
which could contain any number of interlockings. This file describes the
syntax of the interlocking language.

Comments
--------

The ``#`` character begins a *comment*; anything from the ``#``
character to the end of the current source line is ignored by the
program.

``network``
-----------

The ``network`` statement gives the name of the track network being
described in the rest of the file. This must be the first statement in
the file, and must occur exactly once in the file.

::

    network TorontoSubway

defines a track network called ``TorontoSubway``. Any other objects
defined in the file will be created as part of this track network.

Any other statements may occur in any order in the file.

``interlocking``
----------------

The ``interlocking`` statement defines an interlocking, which may
contain any number of levers.

::

    interlocking Finch {
        <levers>
    }

defines an interlocking called ``Finch``, and inside the ``{}`` block is
the set of levers controlled by the interlocking.

``lever``
---------

The ``lever`` statement defines a lever in an interlocking. This
statement is only allowed inside an ``interlocking`` block.

::

    lever 2 {
        normal <condition>
        reverse <condition>
    }

defines a lever numbered ``2``, and the block contains the normal and
reverse conditions (see below). There may be at most one of each
condition; the default is ``0R`` if one is not provided.

A lever number must consist of only digits, and must not be ``0``.

``normal``, ``reverse``
~~~~~~~~~~~~~~~~~~~~~~~

The ``normal`` statement defines the combination of conditions in which
the lever can be set to normal. Similarly, ``reverse`` is the
combination of conditions in which the lever can be set to reverse.
These may only occur inside ``lever``.

If, and only if, these conditions are satisfied, the lever can be
switched to the appropriate state; otherwise, the lever is locked. (In
older interlockings, the interlocking machine physically kept the lever
from being set to the appropriate position.)

See *Lever Conditions* for more information.

``numbering``
-------------

The ``numbering`` statement defines the track numbering convention to be
used when defining subsequent tracks. This may occur more than once in a
file.

::

    numbering Toronto

declares that the following tracks use the Toronto numbering convention
(see *Track Numbering* for more information). Other valid values are
``NewYorkA`` (IRT) and ``NewYorkB`` (BMT/IND).

``track``
---------

The ``track`` statement defines a stretch of track. In S3, a single
stretch of track must have consistent chaining prefixes and numbers
along its entire length; otherwise it must be split into multiple track
objects which can then be attached together (see ``attach``).

::

    track Track1 {
        ...
    }

defines a track object named ``Track1``, the properties of which are
contained in the following block. These properties may depend on the
numbering convention used (see *Track Numbering*), and must occur at
most once for each track.

``up``, ``down``
~~~~~~~~~~~~~~~~

Each ``up`` or ``down`` statement describes the properties of one
direction on the track; specifically, the chaining number of the end of
the track in that direction, and optionally the parity of chaining
numbers of signals in that direction.

In railroad parlance there are only two directions in a rail network,
which I call *up* and *down*; others may call it *north* and *south*, or
*inbound* and *outbound*, possibly depending on the geography of the
network. It is important that the direction of each track be consistent
across the network.

Two forms of the statement are allowed:

::

    up @<offset>

::

    up @<offset> <parity>

In both forms, ``<offset>`` is the chaining number of the end of the
track, in this case the end designated as *up*, and ``<parity>`` is the
parity of the chaining numbers (either ``odd`` or ``even``). If not
provided, the signal numbers are rounded to the nearest integer.

Both ``up`` and ``down`` statements must occur exactly once per track.

``prefix``
~~~~~~~~~~

Defines a chaining prefix on a track. This must occur exactly once on
Toronto numbered tracks, and must not occur on New York numbered tracks.
See *Track Numbering* for more information.

::

    prefix <up> <down>

``chaining line``, ``track number``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Defines the chaining line and track number on a track respectively.
These must both occur exactly once on New York numbered tracks, and must
not occur on Toronto numbered tracks. See *Track Numbering* for more
information.

::

    chaining line <line>

::

    track number <number>

::

    track number <number> <number>

The difference between the latter two forms is that the latter can be
used to define different track numbers for *up* and *down* signals.

``joints``
~~~~~~~~~~

Defines insulated joints along this track, dividing the track into
*track sections* (or *blocks*). Each joint is described as a chaining
number along the track.

::

    joints @<offset> @<offset> # (...)

This statement may occur any number of times within a track.

``switch``
----------

Defines a switch. A switch is "attached" to a certain location on a
track, facing a certain direction, and is controlled by a lever.

::

    switch <track>@<offset> <direction> {
        lever ...
        turnout ...
    }

The switch will be attached to the track named ``<track>`` at the
position given by ``<offset>``, such that the switch can be approached
when traveling in the specified ``<direction>`` (``up`` or ``down``).

``lever``
~~~~~~~~~

Defines which lever controls the switch. This statement may only appear
inside a ``switch`` block.

::

    lever <interlocking>/<lever>

The lever that controls the switch is the lever specified by ``<lever>``
in the interlocking named ``<interlocking>``. If the lever controls more
than one object, then a label can be added to it; for example, if we
want a switch controlled by lever ``15`` in the ``Eglinton``
interlocking to be labeled ``15A``, the appropriate syntax is:

::

    lever Eglinton/15a

(Note the lowercase; interlocking language is case-sensitive.)

``turnout``
~~~~~~~~~~~

Defines the object that can be reached when this switch is thrown. This
can be either the start of a track or another switch. This must only
appear on one of the two objects that need to be connected, and must
appear at most once for each switch.

::

    turnout <interlocking>/<lever>

This form defines a switch on the other side of this switch. This is
especially useful for defining crossovers. The lever reference is the
same as with the ``lever`` statement above.

::

    turnout <track> <direction>

This form says that the switch turns onto the ``<direction>`` (``up`` or
``down``) end of the track named ``<track>``.

``signal``
----------

Defines a signal. A signal is "attached" to a certain location on a
track, facing a certain direction, has a control length, may or may not
be timed, and may or may not be controlled by a lever.

::

    signal <track>@<offset> <direction> {
        lever ...
        approach lever ...
        control length ...
        grade time ...
        station time ...
    }

The signal will be attached to the track named ``<track>``, at the
position given by ``<offset>``, such that the signal can be seen when
traveling in the specified ``<direction>`` (``up`` or ``down``).

``lever``
~~~~~~~~~

Defines the lever that controls this *home* signal. Format is the same
as with ``switch``'s ``lever``. This statement may appear at most once
for each ``signal``, and may not appear together with
``approach lever``.

``approach lever``
~~~~~~~~~~~~~~~~~~

Defines the lever that controls this *approach* signal. Format is the
same as with ``lever``. This statement may appear at most once for each
``signal``, and may not appear together with ``lever``.

``control length``
~~~~~~~~~~~~~~~~~~

The control length of this signal, or the length of track that when
occupied will make this signal not clear (display a red aspect). The
format for control lengths is described in *Control Lengths*.

::

    control length {
        ...
    }

``grade time``, ``station time``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some signals may be grade- or station-timed; these statements define the
parameters of the timing.

::

    grade time {
        ...
    }

::

    station time {
        ...
    }

``time``, ``first time``, ``second time``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The time in seconds for which the signal's timer will run. This is
typically calibrated using the length of the timed section and the speed
limit for that section.

``first time`` and ``second time`` are only for use with two-shot grade
time signals; ``first time`` specifies the time for the first timer,
``second time`` specifies the time for the second timer. For one-shot
grade time signals and station time signals, ``time`` must be used
instead.

::

    time <time>

::

    first time <time>

::

    second time <time>

See *Timing* for more information.

``timed section``, ``first timed section``, ``second timed section``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The section of track that when occupied will activate the timing of the
signal. The format is the same as for ``control length``; see *Control
Lengths* for more information.

As above, ``first timed section`` and ``second timed section`` are used
with two-shot grade time signals, and must both be provided.

::

    timed section {
        ...
    }

::

    first timed section {
        ...
    }

::

    second timed section {
        ...
    }

Lever Conditions
----------------

A *lever condition* is a set of conditions on the state of levers in an
interlocking. The most basic lever condition is whether *one* lever is
in a certain state; for example, ``7N`` ("7 normal") is a condition on
whether the lever numbered ``7`` is set to normal, and ``7R`` ("7
reverse") is whether the lever is set to reverse.

Other, more complex lever conditions are possible. Lever conditions can
be combined using ``&`` and ``|`` logical operators and parentheses:

::

    13N & ((15R & 16N) | (15N & 14N))

There is no equivalent for a logical NOT; a condition can be derived
that does not use NOT while still retaining the original meaning.

In some cases, it may be of use to have an unconditionally true or
unconditionally false lever condition. A dummy lever ``0`` exists for
this purpose. In every interlocking, ``0`` is **always** set to reverse,
so ``0R`` is always true, and ``0N`` is always false.

Track Numbering
---------------

S3 supports three numbering conventions for signals; each signal is
numbered depending on the track it is attached to (this means that
signals on different tracks in the same network may be numbered
differently).

In the descriptions below, a hyphen (``-``) in a signal number indicates
a line break on the signal's number plate.

Toronto (``Toronto``)
~~~~~~~~~~~~~~~~~~~~~

In Toronto numbering, each track has a pair of prefixes; one prefix is
used for signals in the *up* direction, the other for signals in the
*down* direction (the two may be identical). Each direction also has a
parity assigned to it, so that signal numbers are rounded to the nearest
even or odd number.

For example, say we have a track with up prefix ``N`` and down prefix
``S``, with even numbers going up and odd numbers going down. A signal
at chaining number 137+30 facing up would have the signal number
``N-138``, and a signal at 125+60 facing down would be ``S-125``.

For interlocking signals, the lever number is the number of the lever
that controls the signal, with an ``X`` before it, so a signal
controlled by lever ``6`` is ``X-6``. If the lever controls more than
one signal, then each signal has a label which is placed before the
number of the lever, e.g. ``X-A6``, ``X-B6``.

New York BMT/IND (``NewYorkB``)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the BMT/IND numbering, each track has a chaining line, which it
shares with other tracks on the same mainline, and a track number, or in
some cases, two track numbers. Tracks are typically numbered
``1``-``2``, ``1``-``3/4``-``2`` or ``1``-``3``-``4``-``2``, depending
on the number of tracks, where odd tracks go *south* and even tracks go
*north*. In the case where a track has two track numbers, one number is
used for signals going *north* and the other for signals going *south*.
Signal numbers are rounded to the nearest integer.

On a track with chaining line ``B`` and track number ``1``, a signal at
chaining number 137+30 would have the number ``B1-137``. If it were
track ``3/4``, the same signal facing up would be ``B4-137``, and facing
down it would be ``B3-137``.

Interlocking signals are labeled in the same way as Toronto.

New York old IRT (``NewYorkA``)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the IRT numbering, each track also has a chaining line and one or two
track numbers. Tracks are typically numbered ``4``-``3``,
``4``-``1/2``-``3`` or ``4``-``2``-``1``-``3``, depending on the number
of tracks, where odd tracks go *north* and even tracks go *south*.
Similar to BMT/IND numbering, if a track has two numbers, each one is
used for one direction. Signal numbers are rounded to the nearest
integer, and if the signal is facing opposite to the direction of
travel, 4 is added to the chaining number.

(Note that in S3, track numbers are the numbers that appear on the
signals, and not the internally designated track numbers; these two are
different on old IRT sections.)

Signal numbers in IRT numbering consist of the chaining number and the
track number together first, then the chaining line second. On a track
with chaining line ``L`` and track number ``1``, a signal at chaining
number 137+30 facing north would have the number ``1371-L``; if it were
facing south the number would be ``1411-L``.

For interlocking signals, the lever number is the number of the lever
that controls the signal, but the ``X`` comes after: lever ``6`` would
be controlling a signal labeled ``6L-X``. If the lever controls more
than one signal, the labels would be placed right before the ``X``:
``6La-X``, ``6Lb-X``.

Control Lengths
---------------

A *control length* is a stretch of track that, when occupied, will make
a signal display a not-clear indication. The actual length of track that
constitutes the control length may be different depending on what levers
are active in the area of an interlocking; while multiple lengths are
possible, only one can be active at any given time.

Control lengths can be described in S3 by a series of pairs of a lever
condition (see *Lever Conditions* for more information) and a set of
track sections. The system checks each condition in order, and returns
the control length as the set of sections with the *first* corresponding
condition that evaluates to true.

Each pair in a control length has the following format:

::

    <condition> = <track>@<offset>, (...)

where ``<condition>`` is the lever condition, and ``<track>@<offset>``
is a particular offset on the desired section on the named track.

By default, if no condition is specified, or all conditions evaluate to
false, the control length is empty, and empty control lengths are always
**occupied**. A default control length may be specified using an
unconditionally true lever condition (see *Lever Conditions*).

Timing
------

Railways use timed signals to control speed, especially in sections of
track with curves or slopes. Timing is used instead of explicit speed
control, since detecting a train's speed is less trivial.

There are two types of timing in use.

Grade Time
~~~~~~~~~~

This type of timing may e used anywhere there is a curve or grade.
A grade time signal is calibrated to clear at a certain time, based on
the length of the track in front of it and the speed at which it is to
be approached.

There are two sub-types of grade time signals, one-shot and two-shot.

One-shot Grade Time
^^^^^^^^^^^^^^^^^^^

One-shot grade time signals give the train *one shot* (hence the name)
to clear the signal. The timer on a one-shot signal is activated when
the train passes the signal immediately before it, and will clear *if
and only if* the train passes the signal *after* the timer runs out.

One-shot grade time is used in both the Toronto and New York systems.

Parameters of a one-shot signal may be specified using ``time`` and
``control length``.

Two-shot Grade Time
^^^^^^^^^^^^^^^^^^^

Two-shot grade time signals allow two attempts to clear the signal.
Two timers are active in this case.

Take the example scenario below. Signal 3 is the grade time signal,
marked ``GT``.

::

    ------------[========================]+----------------+----
                          -o 1      --->  -o 2          GT -o 3

The first timer is activated when the train passes the signal *two*
signals behind the timed signal, in this case signal 1. If the train
passes signal 2 *after* the timer clears, signal 3 becomes clear and
the train can pass without any further timing.

If the train passes signal 2 *before* the timer clears, it is given
a second attempt to clear the signal. Signal 3 acts much like a
one-shot grade time signal here; the second timer is activated, and
this signal clears if and only if the train passes it after the
timer runs out.

Two shot grade time signals are common in New York, but are virtually
nonexistent in Toronto.

Parameters of a two-shot signal maybe specified using ``first time``,
``second time``, ``first control length``, ``second control length``.

Station Time
~~~~~~~~~~~~

Much like one-shot grade time signals, station time signals have one
timer associated with them. However, ST signals are normally clear,
and the timer will only activate if only the end of its control length
is occupied. As the name implies, this type of timing is typically
used at stations.

Take the scenario below. Signal 1 is a station time signal, and its
control length is denoted by the arrow.

::


    ---------------------------------------------------------------
                                       +-----------+
                                       |  STATION  |
                                       +-----------+
    -----------+-----------+-----------+-----------+----------+----
               -o       ST -o 1        -o          -o 3       -o 4
                           ----------------------->- - - - - ->


Say a train had just left the station, and is now past signal 3:

::

    ------[===============]+-----------+-----------+--------[======
               -o       ST -o 1        -o          -o 3       -o 4
                           ----------------------->- - - - - ->

Since the train is only occupying the *end* of the control length of
signal 1 (the part with the dotted line), the timer can activate and
the next train can safely move into the station.

Station time signals are used in both the Toronto and New York systems.
However, in Toronto they are marked similarly to GT signals.
