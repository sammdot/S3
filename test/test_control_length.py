from unittest import TestCase
from s3.network import TrackNetwork
from s3.interlocking import Interlocking
from s3.lever import Lever, LeverState
from s3.track import Track
from s3.control_length import ControlLength

class ControlLengthTest(TestCase):

	@classmethod
	def setUpClass(cls):
		cls.n = TrackNetwork("Test")
		cls.i = Interlocking("Test", cls.n)
		cls.levers = {i: Lever(i, cls.i) for i in range(10, 15)}
		cls.levers[10].state = LeverState.REVERSE
		cls.levers[11].state = LeverState.REVERSE
		cls.t = Track("Track1", cls.n, up_prefix="B", down_prefix="BA",
			up=0, down=10, joints=[2, 4, 6, 8])

	def test_empty_length(self):
		l = ControlLength([])
		self.assertEqual(l.length, [])
		self.assertTrue(l.occupied)

	def test_only_true_case(self):
		sections = [self.t.sections[-1], self.t.sections[-2]]
		l = ControlLength([(True, sections)])
		self.assertEqual(l.length, sections)

	def test_lever_condition_cases(self):
		# Realistically, lever condition cases are to be used
		# for when switches are set in certain positions so as
		# to allow control length to go to the diverging track.
		sections1 = [self.t.sections[-1], self.t.sections[-2]]
		sections2 = [self.t.sections[-2], self.t.sections[-3]]
		l = ControlLength([
			(self.levers[10].normal | self.levers[12].reverse, sections1),
			(self.levers[10].reverse & self.levers[11].reverse, sections2)
		])
		self.assertEqual(l.length, sections2)

	def test_lever_condition_cases_with_default(self):
		sections1 = [self.t.sections[-1], self.t.sections[-2]]
		sections2 = [self.t.sections[-2], self.t.sections[-3]]
		sections3 = [self.t.sections[-3], self.t.sections[-4]]
		l = ControlLength([
			(self.levers[10].normal | self.levers[12].reverse, sections1),
			(self.levers[10].reverse & self.levers[11].normal, sections2),
			(True, sections3)
		])
		self.assertEqual(l.length, sections3)
