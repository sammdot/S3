from unittest import TestCase
from s3.network import TrackNetwork
from s3.station import Station, StationError, Platform, PlatformError, PlatformSide
from s3.track import Track
from s3.control_length import ControlLength

class PlatformTest(TestCase):

	@classmethod
	def setUpClass(cls):
		cls.n = TrackNetwork("Test")
		cls.t = Track("Track1", cls.n, up_prefix="B", down_prefix="BA", down=16, up=0, joints=[10, 4])
		cls.s = Station("Lawrence", cls.n)

	def test_platform(self):
		l = ControlLength([(True, [self.t.section_with_offset(5)])])
		p = Platform(1, l, PlatformSide.RIGHT)
		self.s.add_platform(p)
		self.assertEqual(p.station, self.s)
		self.assertEqual(p.number, "1")
		self.assertEqual(p.length, l)
		self.assertEqual(p.side, PlatformSide.RIGHT)
		self.assertEqual(self.s.platforms["1"], p)

	def test_duplicate_number(self):
		p = Platform(2, None, PlatformSide.RIGHT)
		self.s.add_platform(p)
		with self.assertRaises(StationError):
			p = Platform(2, None, PlatformSide.RIGHT)
			self.s.add_platform(p)
