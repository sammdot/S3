from unittest import TestCase
from s3.network import TrackNetwork
from s3.direction import Direction
from s3.interlocking import Interlocking
from s3.lever import Lever, LeverState
from s3.switch import Switch, SwitchState, SwitchError
from s3.track import Track
import asyncio

from .async import async

class SwitchTest(TestCase):

	@classmethod
	def setUpClass(cls):
		cls.t = TrackNetwork("test")
		cls.i = Interlocking("Test", cls.t)
		cls.l = Lever(10, cls.i)

	def test_switch(self):
		s = Switch(lever=self.l, label="A")
		self.assertEqual(s.lever, self.l)
		self.assertEqual(s.label, "A")

	def test_no_lever(self):
		with self.assertRaises(SwitchError):
			Switch()

	def test_invalid_lever(self):
		with self.assertRaises(SwitchError):
			Switch(lever=5)

	def test_state(self):
		s = Switch(lever=self.l, label="B")
		self.assertTrue(s.normal)
		self.assertFalse(s.reverse)

	def test_lever(self):
		s = Switch(lever=self.l, label="C")
		self.assertIn(s, self.l)

	def test_location(self):
		s = Switch(lever=self.l, label="D")
		self.assertIsNone(s.track)
		self.assertIsNone(s.position)
		self.assertIsNone(s.direction)

	def test_attach_up(self):
		s = Switch(lever=self.l, label="E")
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		s.attach(t, 0.5, Direction.UP)
		self.assertEqual(s.track, t)
		self.assertEqual(s.position, 0.5)
		self.assertEqual(s.direction, Direction.UP)
		self.assertEqual(s.network, self.t)
		self.assertEqual(t.up_switches[0.5], s)

	def test_attach_down(self):
		s = Switch(lever=self.l, label="F")
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		s.attach(t, 0.5, Direction.DOWN)
		self.assertEqual(s.track, t)
		self.assertEqual(s.position, 0.5)
		self.assertEqual(s.direction, Direction.DOWN)
		self.assertEqual(s.network, self.t)
		self.assertEqual(t.down_switches[0.5], s)

	def test_attach_out_of_range(self):
		s = Switch(lever=self.l, label="G")
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		with self.assertRaises(SwitchError):
			s.attach(t, 2, Direction.DOWN)

	def test_attach_turnout_track(self):
		# UP <-----> DN
		#
		# ---+------ t
		#     \ s
		#      '---- t2
		s = Switch(lever=self.l, label="H")
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		t2 = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		s.attach(t, 0.5, Direction.DOWN)
		s.attach_turnout(t2, Direction.UP)
		self.assertEqual(s.turnout, t2)
		self.assertEqual(t2.up_next, s)

	def test_attach_turnout_switch(self):
		# UP <-----> DN
		#
		# ---+------ t
		#     \ s
		#      \
		#    s2 \
		# -------+-- t2
		s = Switch(lever=self.l, label="J")
		s2 = Switch(lever=self.l, label="K")
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		t2 = Track("Track2", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		s.attach(t, 0.5, Direction.DOWN)
		s2.attach(t2, 0.5, Direction.UP)
		s.attach_turnout(s2)
		self.assertEqual(s.turnout, s2)
		self.assertEqual(s2.turnout, s)

	def test_attach_turnout_to_connected_track(self):
		# UP <-----> DN
		#
		# ---+---------- t
		#     \ s
		#      \
		#       X <- cannot connect here
		# t2 ----+------ t3
		s = Switch(lever=self.l, label="L")
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		t2 = Track("Track2", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		t3 = Track("Track3", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		t2.attach(t3, Direction.DOWN)
		s.attach(t, 0.5, Direction.DOWN)
		with self.assertRaises(SwitchError):
			s.attach_turnout(t3, Direction.UP)

	@async
	def test_switch(self):
		Switch.SWITCH_TIME = 0
		s = Switch(lever=self.l, label="N")
		self.assertTrue(s.normal)
		yield from s.switch(LeverState.REVERSE)
		self.assertTrue(s.reverse)

	def test_locked(self):
		s = Switch(lever=self.l, label="A")
		self.assertFalse(s.locked)
