from unittest import TestCase
from operator import itemgetter
from s3.network import TrackNetwork
from s3.interlocking import Interlocking
from s3.lever import Lever
from s3.direction import Direction
from s3.track import Track
from s3.signal import Signal
from s3.switch import Switch
from s3.control_length import ControlLength

SWO = lambda track, pos: track.section_with_offset(pos)

class RouteTest(TestCase):

	def test_linear_route(self):
		# UP <-----> DN
		#        4            12         16
		# -------+-------------+----------+----
		#        -o 2          -o 4       -o 6
		n = TrackNetwork("Test")
		i = Interlocking("Test", n)
		l2, l4, l6 = [Lever(j, i) for j in [2, 4, 6]]

		t1 = Track("Track1", n, up_prefix="B", down_prefix="B", up=0, down=18, joints=[4, 12, 16])

		s2 = Signal(lever=l2, control_length = ControlLength([
			(True, [SWO(t1, 8), SWO(t1, 14)])
		]))
		s4 = Signal(lever=l4, control_length = ControlLength([
			(True, [SWO(t1, 14)])
		]))
		s6 = Signal(lever=l6)

		s2.attach(t1, 4, Direction.DOWN)
		s4.attach(t1, 12, Direction.DOWN)
		s6.attach(t1, 16, Direction.DOWN)

		routes = i.routes_from(l2)
		self.assertIn(([l2], s4), routes)
		self.assertIn(([l2, l4], s6), routes)
		self.assertEqual(len(routes), 2)

	def test_linear_route_ending_in_automatic(self):
		# UP <-----> DN
		#        4            12         16
		# -------+-------------+----------+----
		#        -o 2          -o 4       -o
		n = TrackNetwork("Test")
		i = Interlocking("Test", n)
		l2 = Lever(2, i)
		l4 = Lever(4, i)

		t1 = Track("Track1", n, up_prefix="B", down_prefix="B", up=0, down=18, joints=[4, 12, 16])

		s2 = Signal(lever=l2, control_length = ControlLength([
			(True, [SWO(t1, 8), SWO(t1, 14)])
		]))
		s4 = Signal(lever=l4, control_length = ControlLength([
			(True, [SWO(t1, 14)])
		]))
		s6 = Signal()

		s2.attach(t1, 4, Direction.DOWN)
		s4.attach(t1, 12, Direction.DOWN)
		s6.attach(t1, 16, Direction.DOWN)

		routes = i.routes_from(l2)
		self.assertIn(([l2], s4), routes)
		self.assertIn(([l2, l4], s6), routes)
		self.assertEqual(len(routes), 2)

	def test_fork_route(self):
		# UP <-----> DN
		#
		#   2          7    10           16  18
		# --+----------+-----+------------+---- t1
		#   -o 2       -o 4   \ 5         -o 6
		#                      '----------+---- t2
		#                                 -o 8
		n = TrackNetwork("Test")
		i = Interlocking("Test", n)
		l2, l4, l5, l6, l8 = [Lever(j, i) for j in [2, 4, 5, 6, 8]]

		t1 = Track("Track1", n, up_prefix="B", down_prefix="B", up=0, down=18, joints=[2, 7, 16])
		t2 = Track("Track2", n, up_prefix="B", down_prefix="B", up=12, down=18)
		sw = Switch(lever=l5, label="A")
		sw.attach(t1, 10, Direction.DOWN)
		sw.attach_turnout(t2, Direction.UP)

		s2 = Signal(lever=l2, control_length = ControlLength([
			(l5.normal, [SWO(t1, 3), SWO(t1, 8)]),
			(l5.reverse, [SWO(t1, 3), SWO(t1, 8), SWO(t2, 15)])
		]))
		s4 = Signal(lever=l4, control_length = ControlLength([
			(l5.normal, [SWO(t1, 8)]),
			(l5.reverse, [SWO(t1, 8), SWO(t2, 15)])
		]))
		s6 = Signal(lever=l6)
		s8 = Signal(lever=l8)

		s2.attach(t1, 2, Direction.DOWN)
		s4.attach(t1, 7, Direction.DOWN)
		s6.attach(t1, 16, Direction.DOWN)
		s8.attach(t2, 16, Direction.DOWN)

		routes = i.routes_from(l2)
		self.assertIn(([l2], s4), routes)
		self.assertIn(([l2, l4], s6), routes)
		self.assertIn(([l2, l4, l5], s8), routes)
		self.assertEquals(len(routes), 3)
