from unittest import TestCase
from s3.network import TrackNetwork
from s3.track import Track, TrackError, TrackSection

class TrackSectionTest(TestCase):

	@classmethod
	def setUpClass(cls):
		cls.n = TrackNetwork("Test")
		cls.t = Track("Track1", cls.n, up_prefix="A", down_prefix="A", up=0, down=10)
		cls.t.joints = {2, 4, 6, 8}

	def test_section_between_joints(self):
		s = TrackSection(self.t, 2, 4)

	def test_end_section(self):
		s = TrackSection(self.t, 0, 2)

	def test_zero_length_section(self):
		with self.assertRaises(TrackError):
			s = TrackSection(self.t, 4, 4)

	def test_section_with_joints(self):
		s = TrackSection(self.t, 4, 8)

	def test_position_in_range(self):
		s = TrackSection(self.t, 0, 2)
		self.assertIn(1, s)

	def test_position_out_of_range(self):
		s = TrackSection(self.t, 0, 2)
		self.assertNotIn(3, s)

	def test_occupancy(self):
		s = TrackSection(self.t, 0, 2)
		self.assertFalse(s.occupied)

	def test_locked(self):
		s = TrackSection(self.t, 2, 4)
		self.assertFalse(s.locked)
