from unittest import TestCase
from s3.network import TrackNetwork

class TrackNetworkTest(TestCase):

	def test_string_id(self):
		t = TrackNetwork("test")
		self.assertEqual(t.id, "test")

	def test_numeric_id(self):
		t = TrackNetwork(17)
		self.assertEqual(t.id, "17")

	def test_interlockings(self):
		t = TrackNetwork("Test")
		self.assertEqual(len(t.interlockings), 0)

	def test_stations(self):
		t = TrackNetwork("Test")
		self.assertEqual(len(t.stations), 0)
