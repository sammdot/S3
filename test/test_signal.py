from unittest import TestCase
from s3.network import TrackNetwork
from s3.direction import Direction
from s3.interlocking import Interlocking
from s3.lever import Lever
from s3.signal import Signal, SignalType, SignalError
from s3.track import Track, NumberingStyle
from s3.control_length import ControlLength
from s3.parity import Parity

class SignalTest(TestCase):

	@classmethod
	def setUpClass(cls):
		cls.t = TrackNetwork("test")
		cls.i = Interlocking("Test", cls.t)
		cls.l = Lever(10, cls.i)

	def test_automatic(self):
		s = Signal()
		self.assertEqual(s.type, SignalType.AUTOMATIC)

	def test_home(self):
		s = Signal(lever=self.l, label="A")
		self.assertEqual(s.type, SignalType.HOME)

	def test_approach(self):
		s = Signal(lever=self.l, approach=True, label="B")
		self.assertEqual(s.type, SignalType.APPROACH)

	def test_approach_without_lever(self):
		with self.assertRaises(SignalError):
			Signal(approach=True)

	def test_invalid_lever(self):
		with self.assertRaises(SignalError):
			Signal(lever=5)

	def test_track_position(self):
		s = Signal()
		self.assertEqual(s.track, None)
		self.assertEqual(s.position, None)

	def test_attach_up(self):
		s = Signal()
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		s.attach(t, 0.5, Direction.UP)
		self.assertEqual(s.track, t)
		self.assertEqual(s.position, 0.5)
		self.assertEqual(s.direction, Direction.UP)
		self.assertIn(0.5, t.up_signals)
		self.assertEqual(t.up_signals[0.5], s)

	def test_attach_down(self):
		s = Signal()
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		s.attach(t, 0.5, Direction.DOWN)
		self.assertEqual(s.track, t)
		self.assertEqual(s.position, 0.5)
		self.assertEqual(s.direction, Direction.DOWN)
		self.assertIn(0.5, t.down_signals)
		self.assertEqual(t.down_signals[0.5], s)

	def test_attach_without_network(self):
		s = Signal()
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		s.attach(t, 0.5, Direction.UP)
		self.assertEqual(s.network, self.t)

	def test_attach_with_lever_without_network(self):
		s = Signal(lever=self.l, label="C")
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		s.attach(t, 0.5, Direction.UP)
		self.assertEqual(s.network, self.t)

	def test_attach_wrong_network(self):
		s = Signal(lever=self.l, label="D")
		n = TrackNetwork("Test")
		t = Track("Track1", n, up_prefix="A", down_prefix="A", up=0, down=1)
		with self.assertRaises(SignalError):
			s.attach(t, 0.5, Direction.UP)

	def test_out_of_range(self):
		s = Signal()
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		with self.assertRaises(SignalError):
			s.attach(t, 3, Direction.UP)

	def test_lever(self):
		s = Signal(lever=self.l, label="E")
		self.assertIn(s, self.l)

	def test_custom_label(self):
		s = Signal(lever=self.l, label="F")
		self.assertEqual(s.label, "F")

	def test_empty_control_length(self):
		s = Signal()
		self.assertEqual(s.control_length, [])

	def test_control_length(self):
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=0, down=1)
		l = ControlLength([(True, t.sections)])
		s = Signal(control_length=l)
		self.assertEqual(s.control_length, t.sections)

	def test_locked(self):
		s = Signal()
		self.assertFalse(s.locked)

	def test_toronto_number(self):
		# UP <-----> DN
		#              BA2 o-
		# 5 ---------------------- 0
		#         -o B3 X12     -o B1
		t = Track("Track1", self.t, up_prefix="BA", down_prefix="B", up=5, down=0,
			up_parity=Parity.EVEN, down_parity=Parity.ODD)
		x12 = Lever(12, self.i)
		s1 = Signal()
		s2 = Signal(lever=x12)
		s3 = Signal()
		s1.attach(t, 1.25, Direction.UP)
		s2.attach(t, 3.6, Direction.DOWN)
		s3.attach(t, 0.2, Direction.DOWN)
		self.assertEqual(s1.number, "BA2")
		self.assertEqual(s2.number, "B3 X12")
		self.assertEqual(s3.number, "B1")

	def test_toronto_multilever_number(self):
		# UP <-----> DN
		#  BA4 XA13 o-          o- BA0 XB13
		# 5 ---------------------- 0
		t = Track("Track1", self.t, up_prefix="BA", down_prefix="B", up=5, down=0,
			up_parity=Parity.EVEN, down_parity=Parity.ODD)
		x13 = Lever(13, self.i)
		s1 = Signal(lever=x13, label="A")
		s2 = Signal(lever=x13, label="B")
		s1.attach(t, 3.6, Direction.UP)
		s2.attach(t, 0, Direction.UP)
		self.assertEqual(s1.number, "BA4 XA13")
		self.assertEqual(s2.number, "BA0 XB13")

	def test_new_york_a_number(self):
		# UP <-----> DN
		#             51-B o-
		# 5 ---------------------> 0
		#         -o 41-B 14LX  -o 01-B
		t = Track("Track1", self.t, line="B", number="1", up=5, down=0,
			down_parity=Parity.ODD, style=NumberingStyle.NEW_YORK_A)
		x14 = Lever(14, self.i)
		s1 = Signal()
		s2 = Signal(lever=x14)
		s3 = Signal()
		s1.attach(t, 1.25, Direction.UP)
		s2.attach(t, 3.6, Direction.DOWN)
		s3.attach(t, 0.2, Direction.DOWN)
		self.assertEqual(s1.number, "51-B")
		self.assertEqual(s2.number, "41-B 14LX")
		self.assertEqual(s3.number, "01-B")

	def test_new_york_a_multilever_number(self):
		# UP <-----> DN
		# 81-B 15LaX o-         o- 41-B 15LbX
		# 5 ---------------------> 0
		t = Track("Track1", self.t, line="B", number="1", up=5, down=0,
			down_parity=Parity.ODD, style=NumberingStyle.NEW_YORK_A)
		x15 = Lever(15, self.i)
		s1 = Signal(lever=x15, label="A")
		s2 = Signal(lever=x15, label="B")
		s1.attach(t, 3.6, Direction.UP)
		s2.attach(t, 0, Direction.UP)
		self.assertEqual(s1.number, "81-B 15LaX")
		self.assertEqual(s2.number, "41-B 15LbX")

	def test_new_york_a_center_track(self):
		# UP <-----> DN
		#     43-D o-
		# 5 <---------------------- 0
		#                    -o 54-D
		t = Track("Track1", self.t, line="D", up_number="3", down_number="4", up=5, down=0,
			up_parity=Parity.ODD, style=NumberingStyle.NEW_YORK_A)
		s1 = Signal()
		s2 = Signal()
		s1.attach(t, 3.6, Direction.UP)
		s2.attach(t, 0.8, Direction.DOWN)
		self.assertEqual(s1.number, "43-D")
		self.assertEqual(s2.number, "54-D")

	def test_new_york_b_number(self):
		# UP <-----> DN
		#             B1-1 o-
		# 5 ---------------------- 0
		#         -o B1-4 X16   -o B1-0
		t = Track("Track1", self.t, line="B", number="1", up=5, down=0,
			style=NumberingStyle.NEW_YORK_B)
		x16 = Lever(16, self.i)
		s1 = Signal()
		s2 = Signal(lever=x16)
		s3 = Signal()
		s1.attach(t, 1.25, Direction.UP)
		s2.attach(t, 3.6, Direction.DOWN)
		s3.attach(t, 0.2, Direction.DOWN)
		self.assertEqual(s1.number, "B1-1")
		self.assertEqual(s2.number, "B1-4 X16")
		self.assertEqual(s3.number, "B1-0")

	def test_new_york_b_multilever_number(self):
		# UP <-----> DN
		# B1-4 XA17 o-          o- B1-0 XB17
		# 5 ---------------------- 0
		t = Track("Track1", self.t, line="B", number="1", up=5, down=0,
			style=NumberingStyle.NEW_YORK_B)
		x17 = Lever(17, self.i)
		s1 = Signal(lever=x17, label="A")
		s2 = Signal(lever=x17, label="B")
		s1.attach(t, 3.6, Direction.UP)
		s2.attach(t, 0, Direction.UP)
		self.assertEqual(s1.number, "B1-4 XA17")
		self.assertEqual(s2.number, "B1-0 XB17")

	def test_new_york_b_center_track(self):
		# UP <-----> DN
		#     D3-4 o-
		# 5 ----------------------- 0
		#                    -o D4-1
		t = Track("Track1", self.t, line="D", up_number="3", down_number="4", up=5, down=0,
			style=NumberingStyle.NEW_YORK_B)
		s1 = Signal()
		s2 = Signal()
		s1.attach(t, 3.6, Direction.UP)
		s2.attach(t, 0.8, Direction.DOWN)
		self.assertEqual(s1.number, "D3-4")
		self.assertEqual(s2.number, "D4-1")

	def test_next_prev_signal(self):
		# UP <-----> DN
		#       o-          o-
		# 5 ----------------------- 0
		t = Track("Track1", self.t, up_prefix="A", down_prefix="A", up=5, down=0)
		s1 = Signal()
		s2 = Signal()
		s1.attach(t, 4, Direction.UP)
		s2.attach(t, 1, Direction.UP)
		self.assertEqual(s1.prev_signal, s2)
		self.assertEqual(s2.next_signal, s1)
