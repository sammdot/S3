from unittest import TestCase
from s3.direction import Direction
from s3.network import TrackNetwork
from s3.interlocking import Interlocking
from s3.lever import Lever
from s3.parity import Parity
from s3.track import Track, TrackError, NumberingStyle
from s3.signal import Signal
from s3.switch import Switch

NYB = NumberingStyle.NEW_YORK_B

class TrackTest(TestCase):

	@classmethod
	def setUpClass(cls):
		cls.n = TrackNetwork("Test")

	def test_string_id(self):
		t = Track("Track1", self.n, style=NYB, line="A", number="1", up=0, down=1)
		self.assertEqual(t.id, "Track1")

	def test_numeric_id(self):
		t = Track(3.5, self.n, style=NYB, line="A", number="1", up=0, down=3.5)
		self.assertEqual(t.id, "3.5")

	def test_network(self):
		t = Track("Track1", self.n, style=NYB, line="A", number="1", up=0, down=1)
		self.assertEqual(t.network, self.n)

	def test_invalid_network(self):
		with self.assertRaises(TrackError):
			t = Track("Track1", None, up=0, down=1)

	def test_network_tracks(self):
		t = Track("Track1", self.n, style=NYB, line="A", number="1", up=0, down=1)
		self.assertIn(t, self.n.tracks)

	def test_track_signals(self):
		t = Track("Track1", self.n, style=NYB, line="A", number="1", up=0, down=1)
		self.assertEqual(len(t.up_signals), 0)
		self.assertEqual(len(t.down_signals), 0)

	def test_track_switches(self):
		t = Track("Track1", self.n, style=NYB, line="A", number="1", up=0, down=1)
		self.assertEqual(len(t.up_switches), 0)
		self.assertEqual(len(t.down_switches), 0)

	def test_no_chaining(self):
		with self.assertRaises(TrackError):
			t = Track("Track1", self.n)

	def test_negative_chaining(self):
		with self.assertRaises(TrackError):
			t = Track("Track1", self.n, style=NYB, line="A", number="1", up=-3, down=1)

	def test_nonnumeric_chaining(self):
		with self.assertRaises(TrackError):
			t = Track("Track1", self.n, style=NYB, line="A", number="1", up="1", down=1)

	def test_digit_track_number(self):
		self.assertTrue(Track.number_is_valid("1"))

	def test_uppercase_track_number(self):
		self.assertTrue(Track.number_is_valid("M"))

	def test_lowercase_track_number(self):
		self.assertTrue(Track.number_is_valid("m"))

	def test_numeric_track_number(self):
		self.assertFalse(Track.number_is_valid(0))

	def test_long_track_number(self):
		self.assertFalse(Track.number_is_valid("12"))

	def test_chaining_line(self):
		self.assertTrue(Track.chaining_line_is_valid("A"))

	def test_long_chaining_line(self):
		self.assertTrue(Track.chaining_line_is_valid("AA"))
	
	def test_longer_chaining_line(self):
		self.assertFalse(Track.chaining_line_is_valid("AAA"))

	def test_digits_chaining_line(self):
		self.assertFalse(Track.chaining_line_is_valid("10"))

	def test_numeric_chaining_line(self):
		self.assertFalse(Track.chaining_line_is_valid(0))

	def test_alphanumeric_chaining_line(self):
		self.assertFalse(Track.chaining_line_is_valid("A1"))

	def test_letter_chaining_prefix(self):
		self.assertTrue(Track.chaining_prefix_is_valid("N"))

	def test_alphanumeric_chaining_prefix(self):
		self.assertTrue(Track.chaining_prefix_is_valid("A1"))
	
	def test_lowercase_chaining_prefix(self):
		self.assertTrue(Track.chaining_prefix_is_valid("n"))

	def test_numeric_chaining_prefix(self):
		self.assertFalse(Track.chaining_prefix_is_valid(0))

	def test_toronto_prefix(self):
		t = Track("Track1", self.n, style=NumberingStyle.TORONTO,
			up_prefix="B", down_prefix="BA", up=0, down=1)
		self.assertEqual(t.up_prefix, "B")
		self.assertEqual(t.down_prefix, "BA")

	def test_new_york_a_prefix(self):
		t = Track("Track1", self.n, style=NumberingStyle.NEW_YORK_A,
			down_parity=Parity.ODD, line="L", number="1", up=0, down=1)
		self.assertEqual(t.up_prefix, None)
		self.assertEqual(t.down_prefix, None)

	def test_new_york_b_prefix(self):
		t = Track("Track1", self.n, style=NumberingStyle.NEW_YORK_B,
			line="A", number="2", up=0, down=1)
		self.assertEqual(t.up_prefix, "A2")
		self.assertEqual(t.down_prefix, "A2")

	def test_next_track(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		self.assertEqual(t.up_next, None)
		self.assertEqual(t.down_next, None)

	def test_no_joints(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		self.assertEqual(len(t.joints), 0)

	def test_valid_joints(self):
		values = [2, 4, 6, 8]
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=10, joints=values)
		self.assertEqual(len(t.joints), 4)
		for i in values:
			self.assertIn(i, t.joints)

	def test_invalid_joint(self):
		with self.assertRaises(TrackError):
			t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1, joints=["a"])

	def test_out_of_range_joint(self):
		with self.assertRaises(TrackError):
			t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1, joints=[2])

	def test_adjacent_sections(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=10, joints=[2, 4, 6, 8])
		jts = sorted(t.sections, key=lambda x: (x.up, x.down))
		j1, j2 = jts[1], jts[2]
		self.assertEqual(j1.up, 2)
		self.assertEqual(j1.down, 4)
		self.assertEqual(j2.up, 4)
		self.assertEqual(j2.down, 6)
		self.assertEqual(j1.down_next, j2)
		self.assertEqual(j2.up_next, j1)

	def test_parity(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1,
			up_parity=Parity.ODD, down_parity=Parity.EVEN)
		self.assertEqual(t.up_parity, Parity.ODD)
		self.assertEqual(t.down_parity, Parity.EVEN)

	def test_default_parity(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		self.assertEqual(t.up_parity, Parity.ODDEVEN)
		self.assertEqual(t.down_parity, Parity.ODDEVEN)

	def test_invalid_parity(self):
		with self.assertRaises(TrackError):
			t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1, up_parity=-1)
		with self.assertRaises(TrackError):
			t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1, down_parity=-1)

	def test_no_joint_sections(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		self.assertEqual(len(t.sections), 1)

	def test_joint_sections(self):
		values = [2, 4, 6, 8]
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=10, joints=values)
		self.assertEqual(len(t.sections), len(values) + 1)

	def test_adjacent_track_sections(self):
		# UP <-----> DN
		#
		#  <--- t1 --> <-- t2 --->
		# +-----+-----+-----+-----+
		#    1     0     1     0
		t1 = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=4, joints=[2])
		t2 = Track("Track2", self.n, up_prefix="A", down_prefix="A", up=0, down=4, joints=[2])
		t1.attach(t2, Direction.DOWN)
		self.assertEqual(t1.sections[0].down_next, t2.sections[1])
		self.assertEqual(t2.sections[1].up_next, t1.sections[0])

	def test_next_track_up(self):
		t1 = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		t2 = Track("Track2", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		t1.attach(t2, Direction.UP)
		self.assertEqual(t1.up_next, t2)
		self.assertEqual(t2.down_next, t1)

	def test_next_track_down(self):
		t1 = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		t2 = Track("Track2", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		t1.attach(t2, Direction.DOWN)
		self.assertEqual(t1.down_next, t2)
		self.assertEqual(t2.up_next, t1)

	def test_invalid_attach(self):
		t1 = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		t2 = Track("Track2", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		with self.assertRaises(TrackError):
			t1.attach(0, Direction.UP)
		with self.assertRaises(TrackError):
			t1.attach(t2, 0)

	def test_in_range(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		self.assertIn(0.5, t)

	def test_out_of_range(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		self.assertNotIn(3, t)

	def test_zero_length(self):
		with self.assertRaises(TrackError):
			t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=0)

	def test_numbering_direction_up(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=1, down=0)
		self.assertEqual(t.direction, Direction.UP)

	def test_numbering_direction_down(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=1)
		self.assertEqual(t.direction, Direction.DOWN)

	def test_section_with_offset_out_of_range(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=5, joints=[2.5])
		self.assertEqual(t.section_with_offset(8), None)

	def test_section_with_offset_joint(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=5, joints=[2.5])
		self.assertEqual(t.section_with_offset(2.5), None)

	def test_section_with_offset_inside_section(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=5, joints=[2.5])
		sec = t.section_with_offset(3)
		self.assertEqual(sec.track, t)
		self.assertEqual(sec.up, 2.5)
		self.assertEqual(sec.down, 5)

	def test_next_up_signal(self):
		# UP <-----> DN
		#          o-    <-
		# +---------+---------+
		#           5     8
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=10, joints=[5])
		s = Signal()
		s.attach(t, 5, Direction.UP)
		self.assertEqual(t.next_signal(8, Direction.UP), s)

	def test_next_down_signal(self):
		# UP <-----> DN
		#       3   5
		# +---------+---------+
		#       ->  -o
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=10, joints=[5])
		s = Signal()
		s.attach(t, 5, Direction.DOWN)
		self.assertEqual(t.next_signal(3, Direction.DOWN), s)

	def test_next_signal_none(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=10, joints=[5])
		self.assertEqual(t.next_signal(8, Direction.UP), None)
		self.assertEqual(t.next_signal(3, Direction.DOWN), None)

	def test_next_signal_after_switch(self):
		# UP <-----> DN
		#     o-     5            10
		# +----+-----+-------------+
		# 0    3      \       8
		#              '-+---------+
		#                6   <-   10
		t1 = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=10, joints=[3])
		t2 = Track("Track2", self.n, up_prefix="B", down_prefix="B", up=6, down=10)
		i = Interlocking("Test", self.n)
		l = Lever(1, i)
		sw = Switch(lever=l, label="A")
		sw.attach(t1, 5, Direction.DOWN)
		sw.attach_turnout(t2, Direction.UP)
		s = Signal()
		s.attach(t1, 3, Direction.UP)
		self.assertEqual(t2.next_signal(8, Direction.UP), s)

	def test_previous_up_signal(self):
		# UP <-----> DN
		#      <-  o-
		# +---------+---------+
		#       3   5
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=10, joints=[5])
		s = Signal()
		s.attach(t, 5, Direction.UP)
		self.assertEqual(t.prev_signal(3, Direction.UP), s)

	def test_previous_down_signal(self):
		# UP <-----> DN
		#           5     8
		# +---------+---------+
		#           -o    ->
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=10, joints=[5])
		s = Signal()
		s.attach(t, 5, Direction.DOWN)
		self.assertEqual(t.prev_signal(8, Direction.DOWN), s)

	def test_prev_signal_none(self):
		t = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=10, joints=[5])
		self.assertEqual(t.prev_signal(8, Direction.UP), None)
		self.assertEqual(t.prev_signal(3, Direction.DOWN), None)

	def test_prev_signal_after_switch(self):
		# UP <-----> DN
		# 0    3     5            10
		# +----+-----+-------------+
		#      -o    \       8
		#              '-+---------+
		#                6   ->   10
		t1 = Track("Track1", self.n, up_prefix="A", down_prefix="A", up=0, down=10, joints=[3])
		t2 = Track("Track2", self.n, up_prefix="B", down_prefix="B", up=6, down=10)
		i = Interlocking("Test", self.n)
		l = Lever(1, i)
		sw = Switch(lever=l, label="A")
		sw.attach(t1, 5, Direction.DOWN)
		sw.attach_turnout(t2, Direction.UP)
		s = Signal()
		s.attach(t1, 3, Direction.DOWN)
		self.assertEqual(t2.prev_signal(8, Direction.DOWN), s)
