import asyncio
import functools

def async(f):
	@functools.wraps(f)
	def wrapper(*args, **kwargs):
		coro = asyncio.coroutine(f)
		future = coro(*args, **kwargs)
		_loop = asyncio.get_event_loop()
		loop = asyncio.new_event_loop()
		asyncio.set_event_loop(loop)
		loop.run_until_complete(future)
		loop.close()
		asyncio.set_event_loop(_loop)
	return wrapper
