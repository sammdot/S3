from unittest import TestCase
from s3.parser import Token, TokenType

class TokenTest(TestCase):

	def test_token(self):
		t = Token(TokenType.NEWLINE, (None, None, None), "\n")
		self.assertEqual(t.type, TokenType.NEWLINE)
		self.assertEqual(t.value, "\n")

	def test_token_with_no_value(self):
		t = Token(TokenType.AT, (None, None, None))
		self.assertEqual(t.type, TokenType.AT)
		self.assertEqual(t.value, None)
