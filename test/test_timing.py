from unittest import TestCase
from s3.control_length import ControlLength
from s3.timing import Timing, TimingType, TimingError

class TimingTest(TestCase):

	def test_grade_time(self):
		t = Timing(time=10)
		self.assertEqual(t.type, TimingType.GRADE)

	def test_station_time(self):
		t = Timing(time=10, type=TimingType.STATION)
		self.assertEqual(t.type, TimingType.STATION)
	
	def test_no_time(self):
		with self.assertRaises(TimingError):
			Timing()

	def test_empty_timed_section(self):
		t = Timing(time=10)
		self.assertEqual(t.timed_section, [])

	def test_timed_section(self):
		l = ControlLength([(True, [0])]) # [0] is a placeholder
		t = Timing(time=10, timed_section=l)
		self.assertEqual(t.timed_section, [0])

	def test_time(self):
		t = Timing(time=10)
		self.assertEqual(t.time, 10)
		self.assertIs(t.first_time, None)

	def test_first_time(self):
		t = Timing(time=10, first_time=12)
		self.assertEqual(t.first_time, 12)

	def test_two_timer_station_time(self):
		with self.assertRaises(TimingError):
			t = Timing(type=TimingType.STATION, time=10, first_time=12)
