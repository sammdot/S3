from unittest import TestCase
from s3.network import TrackNetwork
from s3.station import Station, StationError, Platform

class StationTest(TestCase):

	@classmethod
	def setUpClass(cls):
		cls.n = TrackNetwork("Test")

	def test_name(self):
		s = Station("Finch", self.n)
		self.assertEqual(s.name, "Finch")

	def test_numeric_name(self):
		s = Station(17, self.n)
		self.assertEqual(s.name, "17")

	def test_empty_platforms(self):
		s = Station("Sheppard", self.n)
		self.assertEqual(s.platforms, {})

	def test_network(self):
		s = Station("York Mills", self.n)
		self.assertEqual(s.network, self.n)
		self.assertIn(s, self.n.stations)
