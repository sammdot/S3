from unittest import TestCase
from s3.network import TrackNetwork
from s3.interlocking import Interlocking
from s3.lever import Lever, LeverState, LeverStateCondition, And, Or, LeverStateError

class LeverConditionTest(TestCase):

	@classmethod
	def setUpClass(cls):
		cls.t = TrackNetwork("test")
		cls.i = Interlocking("Test", cls.t)

	def test_normal(self):
		l = Lever(10, self.i)
		self.assertTrue(l.normal)

	def test_reverse(self):
		l = Lever(11, self.i)
		self.assertFalse(l.reverse)

	def test_and(self):
		l = Lever(12, self.i)
		l2 = Lever(13, self.i)
		self.assertFalse(l.normal & l2.reverse)

	def test_or(self):
		l = Lever(14, self.i)
		l2 = Lever(15, self.i)
		self.assertTrue(l.normal | l2.reverse)

	def test_invalid_state(self):
		l = Lever(16, self.i)
		with self.assertRaises(LeverStateError):
			LeverStateCondition(l, 0)

	def test_nested_and_or(self):
		l1 = Lever(17, self.i)
		l2 = Lever(18, self.i)
		l3 = Lever(19, self.i)
		self.assertTrue((l1.normal & l2.normal) & l3.normal)
		self.assertTrue((l1.normal & l2.normal) | l3.normal)
		self.assertTrue((l1.normal | l2.normal) & l3.normal)
		self.assertTrue((l1.normal | l2.normal) | l3.normal)
