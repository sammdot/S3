from unittest import TestCase
from s3.network import TrackNetwork
from s3.interlocking import Interlocking
from s3.lever import Lever, LeverError, LeverState
from s3.switch import Switch
import asyncio

from .async import async

class MockSwitch:
	def __init__(self):
		self.label = ""

class LeverTest(TestCase):

	@classmethod
	def setUpClass(cls):
		cls.t = TrackNetwork("test")
		cls.i = Interlocking("Test", cls.t)

	def test_string_id(self):
		l = Lever("10", self.i)
		self.assertEqual(l.id, 10)

	def test_numeric_id(self):
		l = Lever(11, self.i)
		self.assertEqual(l.id, 11)

	def test_invalid_id(self):
		with self.assertRaises(LeverError):
			Lever("40A", self.i)
	
	def test_negative_id(self):
		with self.assertRaises(LeverError):
			Lever(-3, self.i)

	def test_existing_lever(self):
		Lever(15, self.i)
		with self.assertRaises(LeverError):
			Lever(15, self.i)

	def test_network(self):
		l = Lever(16, self.i)
		self.assertIs(l.interlocking, self.i)
		self.assertIs(l.network, self.t)

	def test_interlocking_levers(self):
		l = Lever(18, self.i)
		self.assertIn(l, self.i.levers)

	def test_state(self):
		l = Lever(19, self.i)
		self.assertTrue(l.normal)
		self.assertFalse(l.reverse)

	def test_duplicate_label(self):
		l = Lever(20, self.i)
		l.add(MockSwitch(), "A")
		with self.assertRaises(LeverError):
			l.add(MockSwitch(), "A")

	def test_invalid_label(self):
		l = Lever(21, self.i)
		with self.assertRaises(LeverError):
			l.add(MockSwitch(), "0")

	@async
	def test_switch(self):
		l = Lever(22, self.i)
		Switch.SWITCH_TIME = 0
		s = Switch(lever=l, label="A")
		l.normal_if(True)
		l.reverse_if(True)
		self.assertTrue(l.normal)
		yield from l.switch()
		self.assertTrue(l.reverse)
