from unittest import TestCase
from s3.network import TrackNetwork
from s3.interlocking import Interlocking
from s3.lever import Lever
from s3.signal import Signal

class InterlockingTest(TestCase):

	@classmethod
	def setUpClass(cls):
		cls.t = TrackNetwork("test")

	def test_alpha_id(self):
		i = Interlocking("Test", self.t)
		self.assertEqual(i.id, "Test")
	
	def test_digit_id(self):
		i = Interlocking(10, self.t)
		self.assertEqual(i.id, "10")

	def test_levers(self):
		i = Interlocking("Test2", self.t)
		self.assertEqual(len(i.levers), 0)

	def test_network(self):
		i = Interlocking("Test3", self.t)
		self.assertIs(i.network, self.t)
		self.assertIn(i, self.t.interlockings)

	def test_one_signal_levers(self):
		i = Interlocking("Test4", self.t)

		l1 = Lever(1, i)
		s1 = Signal(lever=l1)

		l2 = Lever(2, i)
		s2 = Signal(lever=l2, label="A")
		s3 = Signal(lever=l2, label="B")

		levers = i.one_signal_levers
		self.assertIn(l1, levers)
		self.assertNotIn(l2, levers)
