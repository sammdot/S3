from PySide.QtCore import *
from PySide.QtGui import *

from .painter import Painter

class InterlockingWidget(QWidget):

	def __init__(self, parent, layout, bbox, interlocking):
		QWidget.__init__(self, parent)
		self._layout = layout

		self._interlocking = interlocking
		self._bbox = bbox

	def paintEvent(self, e):
		with Painter(self) as qp:
			self.render(qp)

	def render(self, qp):
		pen = QPen(QColor(255, 255, 255))
		pen.setWidth(0.5)
		qp.setPen(pen)
		qp.drawRect(self._bbox)

		f = QFont("Helvetica", 12)
		f.setWeight(QFont.Bold)
		f.setStyleHint(QFont.SansSerif)
		f.setHintingPreference(QFont.HintingPreference.PreferNoHinting)
		qp.setFont(f)
		point = self._bbox.topLeft()
		point.setY(point.y() - 8)
		qp.drawText(point, self._interlocking.id)
