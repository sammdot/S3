from PySide.QtCore import *
from PySide.QtGui import *

from .painter import Painter
from .clickable import ClickableWidget

from .window import ControlWindow
from .layout_panel import LayoutPanel
from .switch_widget import SwitchWidget
from .track_widget import TrackWidget
from .interlocking_widget import InterlockingWidget

from s3.interlocking import Interlocking
from s3.track import Track, TrackSection
from s3.switch import Switch

import asyncio
import svg.path as svgpath

class MainLayoutPanel(LayoutPanel):

	def __init__(self, root):
		LayoutPanel.__init__(self, root)

		self._network = None
		self._layout = None

		self._interlockings = {}
		self._interlocking_widgets = {}
		self.sub_windows = []

	async def draw(self, network, layout, viewport, scale):
		self._network = network
		self._layout = layout

		self._viewport = viewport[0] + viewport[1]
		self._scale = scale
		self.scale = scale
		self._resize()

		# TODO: actually draw the things
		for obj in layout.keys():
			if isinstance(obj, Interlocking):
				l, t, r, b = self.transform(layout[obj][0]) + self.transform(layout[obj][1])
				self._interlockings[obj] = QRect(l, t, r - l, b - t)
			elif isinstance(obj, Switch):
				self._switches[obj] = layout[obj]
			elif isinstance(obj, TrackSection):
				self._tracks[obj] = svgpath.parse_path(layout[obj])

		for intg, bbox in self._interlockings.items():
			wid = self._interlocking_widgets[intg] = InterlockingWidget(self.layout, self, bbox, intg)
		for track, path in self._tracks.items():
			wid = self._track_widgets[track] = TrackWidget(self.layout, self, path, track)

		for switch, params in self._switches.items():
			if switch in self._switch_widgets:
				continue
			direction, offset = params
			track = switch.track.section_with_offset(switch.position)
			wid = self._switch_widgets[switch] = SwitchWidget(self.layout, self, switch, \
				self._track_widgets.get(track), direction, offset)
			if isinstance(switch.turnout, Switch):
				switch2 = switch.turnout
				direction, offset = self._switches[switch2]
				track = switch2.track.section_with_offset(switch2.position)
				wid2 = self._switch_widgets[switch2] = SwitchWidget(self.layout, self, switch2, \
					self._track_widgets.get(track), direction, offset)
				wid.turnout_wid = wid2
			elif isinstance(switch.turnout, Track):
				track2 = switch.turnout
				other_point = None
				if track2.up_next == switch:
					other_point = self._track_widgets[track2.sections[-1]].raw_path[0].start
				elif track2.down_next == switch:
					other_point = self._track_widgets[track2.sections[0]].raw_path[-1].end
				if not other_point:
					continue

				wid.turnout_point = self.transform((other_point.real, other_point.imag))

	def _on_click(self, x, y):
		interlockings = [intg for intg in self._interlockings.keys() if \
			self._interlockings[intg].contains(QPoint(x, y))]

		if len(interlockings) == 1:
			intg = interlockings[0]
			win = ControlWindow(intg)
			win.setWindowTitle(intg.id)
			asyncio.ensure_future(win.layout.draw(self._network, self._layout, \
				self._layout[intg], self._scale * 2))
			win.show()
			self.sub_windows.append(win)
