from PySide.QtCore import *
from PySide.QtGui import *


class Bookmark:

	def __init__(self, name, host, port):
		self.name = name
		self.host = host
		self.port = port

	def __repr__(self):
		return "<Bookmark {0}>".format(self.name)


class BookmarkModel(QAbstractListModel):

	def __init__(self, bookmarks):
		QAbstractListModel.__init__(self)
		self.bookmarks = bookmarks

	def rowCount(self, parent=QModelIndex()):
		return len(self.bookmarks)

	def data(self, index, role=Qt.DisplayRole):
		if role == Qt.DisplayRole:
			return QVariant(self.bookmarks[index.row()])
		else:
			return QVariant()
