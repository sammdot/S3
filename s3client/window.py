from PySide.QtCore import *
from PySide.QtGui import *

import asyncio

from .layout_panel import LayoutPanel
from .console import ConsolePanel

class ControlWindow(QWidget):

	def __init__(self, intg):
		QWidget.__init__(self)

		self.create_menu()

		self._layout = QVBoxLayout(self)
		self.setLayout(self._layout)

		self.layout = LayoutPanel(self)
		self._layout.addWidget(self.layout, 1)

	def create_menu(self):
		menus = [
			# TODO: menu items specific to individual interlockings
		]

		menubar = QMenuBar()

		for name, items in menus:
			submenu = menubar.addMenu(name)
			for label, function in items:
				if label is None:
					submenu.addSeparator()
				else:
					action = QAction(label, submenu)
					submenu.addAction(action)
					action.triggered.connect(function)

		# self._layout.addWidget(menubar)
