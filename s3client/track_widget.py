from PySide.QtCore import *
from PySide.QtGui import *

from .painter import Painter
from .clickable import ClickableWidget

import svg.path as svgpath

class TrackWidget(ClickableWidget):

	def __init__(self, parent, layout, path, track):
		ClickableWidget.__init__(self, parent)
		self._layout = layout

		self._track = track
		track.on_change_occupied(lambda trk: self.update())

		self.raw_path = path
		self.path = _path = QPainterPath()
		for i in range(len(path)):
			seg = path[i]
			if isinstance(seg, svgpath.Line):
				_path.moveTo(*layout.transform((seg.start.real, seg.start.imag)))
				_path.lineTo(*layout.transform((seg.end.real, seg.end.imag)))
			# TODO: other path object types

		self.path = _path

	def paintEvent(self, e):
		with Painter(self) as qp:
			self.render(qp)

	def render(self, qp):
		w, h = self.size()

		pen = QPen(QColor(255, 0, 0) if self._track.occupied else QColor(255, 255, 255))
		pen.setWidth(self._layout.scale / 3)
		qp.setPen(pen)
		qp.drawPath(self.path)
