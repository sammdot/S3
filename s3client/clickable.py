from PySide.QtCore import *
from PySide.QtGui import *

class ClickableWidget(QWidget):

	"""
	A helper class for clickable widgets.
	Defines a 'clicked' signal.
	"""

	clicked = Signal(int, int)

	def __init__(self, parent):
		QWidget.__init__(self, parent)

	def mouseReleaseEvent(self, e):
		if e.button() == Qt.LeftButton:
			self.update()
			self.clicked.emit(e.x(), e.y())
			e.accept()

	def size(self):
		"""
		Return the size of the widget as
		a tuple instead of a QSize.
		"""
		size = QWidget.size(self)
		return size.width(), size.height()
