from PySide.QtCore import *
from PySide.QtGui import *

class ConsolePanel(QPlainTextEdit):

	def __init__(self, root, height, bg, fg, font, fontsize):
		QPlainTextEdit.__init__(self, root)
		self.setReadOnly(True)
		self.setTabStopWidth(4)

		self.background = bg
		self.foreground = fg

		self.setStyleSheet("QPlainTextEdit {{ background-color: {0}; color: {1}; }}".format(bg, fg))
		self.setMaximumHeight(height)
		self.setMinimumHeight(height)
		self.setWordWrapMode(QTextOption.NoWrap)

		self.font = None
		if font != "default":
			self.font = QFont(font, fontsize)
			self.setFont(self.font)

	def clear(self):
		self.clear()

	def write(self, text):
		self.moveCursor(QTextCursor.End)
		self.insertPlainText(text)
		self.verticalScrollBar().setValue(100);
		self.horizontalScrollBar().setValue(0);

	def flush(self):
		pass
