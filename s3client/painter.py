from PySide.QtCore import *
from PySide.QtGui import *

class Painter:

	"""
	A wrapper for the QPainter class which lets us
	use it with context managers ('with' statement).
	"""

	def __init__(self, widget):
		self.painter = QPainter()
		self.widget = widget

	def __enter__(self, *args):
		self.painter.begin(self.widget)
		self.painter.setRenderHints(
			QPainter.Antialiasing |
			QPainter.TextAntialiasing |
			QPainter.SmoothPixmapTransform)
		return self.painter

	def __exit__(self, *args):
		self.painter.end()
