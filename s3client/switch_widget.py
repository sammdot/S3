from PySide.QtCore import *
from PySide.QtGui import *

from .painter import Painter
from .clickable import ClickableWidget

class SwitchWidget(ClickableWidget):

	def __init__(self, parent, layout, switch, track, direction, offset):
		self.label = False
		self._point = (-1, -1)
		if track:
			pt = track.raw_path.point(offset)
			self._point = layout.transform((pt.real, pt.imag))
		self.center = self._point

		ClickableWidget.__init__(self, parent)
		self._layout = layout

		self._switch = switch
		self._track_wid = track
		self._direction = direction
		self._offset = offset

		self.turnout_wid = None
		self.turnout_point = None

	def paintEvent(self, e):
		with Painter(self) as qp:
			self.render(qp)

	def render(self, qp):
		if self._point == (-1, -1):
			return

		if self.label:
			qp.setPen(QColor(255, 255, 255))
			point2 = QPoint(self._point[0] - 3, self._point[1] - 5)
			qp.drawText(point2, str(self._switch).split("/")[1])

		if self.turnout_wid:
			tw = self.turnout_wid
			if isinstance(tw, SwitchWidget):
				pen = QPen(QColor(255, 255, 255))
				pen.setWidth(self._layout.scale / 3)
				qp.setPen(pen)
				qp.drawLine(QPoint(*self.center), QPoint(*tw.center))
		elif self.turnout_point:
			pen = QPen(QColor(255, 255, 255))
			pen.setWidth(self._layout.scale / 3)
			qp.setPen(pen)
			qp.drawLine(QPoint(*self.center), QPoint(*self.turnout_point))

		if self._layout.scale > 5:
			qp.setBrush(QColor(0, 0, 0))
			point = QPoint(*self._point)
			qp.setPen(None)
			radius = self._layout.scale * 2
			qp.drawEllipse(point, radius, radius)

			if self._switch.normal:
				self._track_wid.render(qp)
