from s3.protocol import Message
import asyncio

class ClientError(Exception):
	pass

class Client:

	def __init__(self, host, port, ssl=None):
		self.host = host
		self.port = port
		self.ssl = ssl

		self.reader = None
		self.writer = None

		self.loop = None

		self._observers = []

	async def start(self):
		try:
			self.loop = asyncio.get_event_loop()
			self.reader, self.writer = await asyncio.open_connection(
				host=self.host, port=self.port, ssl=self.ssl)
		except Exception as e:
			raise ClientError(*e.args)

		while True:
			line = (await self.reader.readline()).decode("utf-8").strip()
			if line == "":
				break

			msg = Message.from_line(line)
			self.notify_observers(msg)

	def stop(self):
		if self.loop is None:
			return
		self.loop.call_soon_threadsafe(self.loop.stop)

	def send(self, msg):
		self.writer.write("{0}\n".format(msg).encode("utf-8"))

	def add_observer(self, func):
		self._observers.append(func)

	def remove_observer(self, func):
		if func not in self._observers:
			return
		self._observers.remove(func)

	def notify_observers(self, msg):
		for func in self._observers:
			func(msg)
