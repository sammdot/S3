import asyncio
import zlib

class DownloadClientError(Exception):
	pass


class DownloadClient:

	def __init__(self, host, port, ssl=None):
		self.host = host
		self.port = port
		self.ssl = ssl

		self.reader = None
		self.writer = None

		self.loop = None

	async def download(self):
		try:
			self.loop = asyncio.get_event_loop()
			self.reader, self.writer = await asyncio.open_connection(
				host=self.host, port=self.port, ssl=self.ssl)
		except Exception as e:
			raise DownloadClientError(*e.args)

		return zlib.decompress(await self.reader.read()).decode("utf-8")
