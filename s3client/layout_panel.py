from PySide.QtCore import *
from PySide.QtGui import *

from .painter import Painter
from .clickable import ClickableWidget

from .track_widget import TrackWidget

from s3.track import TrackSection
from s3.switch import Switch

import svg.path as svgpath

class LayoutWidget(ClickableWidget):

	def __init__(self, parent):
		ClickableWidget.__init__(self, parent)
		self.clicked.connect(parent._on_click)

	def paintEvent(self, e):
		with Painter(self) as qp:
			qp.setBrush(QColor(0, 0, 0))
			qp.drawRect(self.rect())

			for child in self.children():
				child.render(qp)

class LayoutPanel(QScrollArea):

	def __init__(self, root):
		QScrollArea.__init__(self, root)

		self.layout = LayoutWidget(self)
		self.setAlignment(Qt.AlignCenter)
		self.setWidget(self.layout)
		self.setStyleSheet("QScrollArea { background-color: black; }")

		self._viewport = (0, 0, 0, 0)
		self._scale = 1

		self._tracks = {}
		self._switches = {}

		self._track_widgets = {}
		self._switch_widgets = {}

	def _resize(self):
		l, t, r, b = self._viewport

		if (b > t and r < l) or (b < t and r > l):
			w, h = abs(b - t), abs(r - l)
		else:
			w, h = abs(r - l), abs(b - t)
		self.layout.resize(self._scale * w, self._scale * h)

	def transform(self, coords):
		"""
		Transform world coordinates to viewport coordinates,
		depending on this panel's viewport rectangle.
		"""

		def _transform_raw():
			l, t, r, b = self._viewport
			x, y = coords

			if b > t:
				if r > l:
					return (x - l, y - t)
				elif r < l:
					return (y - t, l - x)
			elif b < t:
				if r > l:
					return (t - y, x - l)
				elif r < l:
					return (l - x, t - y)
			return (0, 0)

		x, y = _transform_raw()
		return (self._scale * x, self._scale * y)

	async def draw(self, network, layout, viewport, scale):
		self._viewport = viewport[0] + viewport[1]
		self._scale = scale
		self.scale = scale
		self._resize()

		# TODO: actually draw the things
		for obj in layout.keys():
			if isinstance(obj, Switch):
				self._switches[obj] = layout[obj]
			elif isinstance(obj, TrackSection):
				self._tracks[obj] = svgpath.parse_path(layout[obj])

		for track, path in self._tracks.items():
			wid = self._track_widgets[track] = TrackWidget(self.layout, self, path, track)

	def _on_click(self, x, y):
		pass
