from collections import deque
from .direction import Direction

class InterlockingError(Exception):
	pass

class Interlocking:
	"""
	An arrangement of levers, signals and switches that only allows
	safe movements through a rail junction or crossing.
	"""

	def __init__(self, id, net):
		self.id = str(id)
		self.levers = set()

		self.network = net
		net.interlockings.add(self)

	def lever(self, id):
		levers = [l for l in self.levers if l.id == id]
		return levers[0] if levers != [] else None

	def __repr__(self):
		return "<Interlocking {0}>".format(self.id)

	def __str__(self):
		return self.id

	@property
	def one_signal_levers(self):
		from .signal import Signal
		controls_exactly_one_signal = lambda lvr: len(lvr.controls.keys()) == 1 \
			and isinstance(list(lvr.controls.values())[0], Signal)
		return list(filter(controls_exactly_one_signal, self.levers))

	def routes_from(self, lever):
		if lever not in self.one_signal_levers:
			# This will automatically exclude levers
			# that are not part of this interlocking.
			return None

		from .track import TrackSection
		from .switch import Switch

		routes = []
		routes_queue = deque()
		routes_queue.append([lever])

		levers = self.one_signal_levers

		while routes_queue:
			route = routes_queue.popleft()
			last_lever = [lever for lever in route if lever in levers][-1]
			last_sig = list(last_lever.controls.values())[0]
			next_sig = last_sig.next_signal
			if next_sig:
				routes.append((route, next_sig))

			if next_sig and next_sig.lever:
				routes_queue.append(route + [next_sig.lever])

			if last_sig.track.no_signals_until_end(last_sig.position, last_sig.direction):
				_next = last_sig.track.up_next if last_sig.direction == Direction.UP \
					else last_sig.track.down_next
				if isinstance(_next, Switch):
					new_route = route + [_next.lever]
					if next_sig:
						new_route.append(next_sig.lever)
					routes_queue.append(new_route)

			sec = last_sig.track.section_after(last_sig)
			switches = list((sec.up_switches if last_sig.direction == Direction.UP \
				else sec.down_switches).values())
			for switch in switches:
				turnout = switch.turnout

				next_sig = switch.next_signal
				if next_sig:
					routes.append((route + [switch.lever], next_sig))
					if next_sig.lever:
						routes_queue.append(route + [switch.lever, next_sig.lever])

		return routes
