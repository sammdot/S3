from enum import Enum
from .network import TrackNetwork
from .track import Track

class PlatformSide(Enum):
	LEFT = 1
	RIGHT = 2

class PlatformError(Exception):
	pass

class Platform:
	"""
	One platform in a train station. An island
	platform counts as two separate platforms.
	"""

	def __init__(self, number, control_length, side):
		self.station = None
		self.number = str(number)

		# The length of the station platform
		# expressed as a control length.
		self.length = control_length
		# The side the platform is on, relative to UP.
		self.side = side

	def __repr__(self):
		return "<Platform {0} of {1}>".format(self.number, self.station)

class StationError(Exception):
	pass

class Station:

	def __init__(self, name, network, platforms=None):
		self.name = str(name)
		self.platforms = {}
		for platform in platforms or []:
			self.add_platform(platform)

		self.network = network
		network.stations.add(self)

	def __repr__(self):
		return "<Station {0}>".format(self.name)

	def add_platform(self, platform):
		if platform.number in self.platforms.keys():
			raise StationError("numbered platform already exists: " + platform.number)
		self.platforms[platform.number] = platform
		platform.station = self
