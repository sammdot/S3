class Logger:

	def __init__(self, filename, warn=False):
		self.filename = filename
		self.errors = 0
		self._warn = warn

	def show_message(self, type, msg, obj=None, lineno=None, file=None):
		if file is None:
			import sys
			file = sys.stderr

		if obj is not None:
			lineno = obj.start[0]
			print("{0}:{1}: {2}: {3}".format(self.filename, lineno, type, msg), file=file)
			print("    {0}".format(self.lines[lineno].replace("\t", " ")), file=file)
			print("    {0}^".format(" " * (obj.start[1] - 1)), file=file)
		elif lineno is not None:
			print("{0}:{1}: {2}: {3}".format(self.filename, lineno, type, msg), file=file)

	def note(self, msg, obj):
		if self._warn:
			self.show_message("note", msg, obj=obj)

	def warning(self, msg, obj):
		if self._warn:
			self.show_message("warning", msg, obj=obj)

	def error(self, msg, obj):
		self.errors += 1
		self.show_message("error", msg, obj=obj)

	def syntax_error(self, msg, obj):
		self.error(msg, obj)
		raise Exception
