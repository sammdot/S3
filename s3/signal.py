from enum import Enum
from .control_length import ControlLength
from .direction import Direction
from .parity import Parity
from .lever import Lever, LeverState
from .track import Track, NumberingStyle
import asyncio

class SignalState(Enum):
	"""
	Possible states of a signal. NOT_CLEAR and CLEAR are the
	main states; CLEARING is only an intermediate state and
	signals cannot be locked into this state.
	"""

	NOT_CLEAR = 1
	CLEARING = 2
	CLEAR = 3

class SignalType(Enum):
	"""
	Possible types of signals. In a typical NX-type system,
	these are the types of signals available.
	"""

	AUTOMATIC = 1
	APPROACH = 2
	HOME = 3

class SignalError(Exception):
	pass

class Signal:
	"""
	An individual signal in a track layout. We usually use a
	color-light signal to show its state, but any other type
	of signal can be used.
	"""

	def __init__(self, label="A", lever=None, approach=False,
		control_length=None, grade_time=None, station_time=None):
		# The lever that controls this signal. If there is a lever
		# provided, the signal is either APPROACH or HOME, otherwise
		# it is AUTOMATIC.
		self.lever = None
		if lever is None:
			self.type = SignalType.AUTOMATIC
			if approach:
				raise SignalError("lever required for approach signals")
		elif isinstance(lever, Lever):
			self.type = SignalType.APPROACH if approach else \
				SignalType.HOME
			self.lever = lever
			lever.add(self, label)
		else:
			raise SignalError("invalid lever: {0}".format(lever))

		# Set the state to NOT_CLEAR by default; once the
		# system is initialized this may be set to CLEAR.
		self._state = SignalState.NOT_CLEAR
		self._state_observers = []

		self._locked = False
		self._locked_observers = []

		self._manual = False
		self._manual_observers = []

		# The position of the signal on a track. This is not
		# set until we use the attach() method below.
		self.track = None
		self.position = None
		self.direction = None

		self.network = None

		# The length of track that if occupied will turn this signal red.
		self._control_length = control_length or ControlLength([])
		for sec in self._control_length.sections:
			sec.on_change_occupied(self.on_occupancy_change)

		self.grade_time = grade_time
		if grade_time is not None:
			for sec in grade_time.sections:
				sec.on_change_occupied(self.on_GT_section_change)
			for sec in grade_time.first_sections:
				sec.on_change_occupied(self.on_2GT_section_change)

		self.station_time = station_time
		if station_time is not None:
			for sec in station_time.sections:
				sec.on_change_occupied(self.on_ST_section_change)

	@property
	def _number_parts(self):
		number = ""
		suffix = None

		if self.lever:
			label = self.label
			# If this is the only one controlled by the lever,
			# do not include the label in the suffix.
			if len(self.lever.controls.values()) == 1:
				label = ""

			suffix = (self.lever.id, label if self.track is not None else "")

		if self.track is None:
			return (None, suffix)

		if self.track.style == NumberingStyle.NEW_YORK_A:
			chaining = round(self.position)
			if self.track.travel_direction != self.direction:
				chaining += 4
			tknum = self.track.up_number if self.direction == Direction.UP else self.track.down_number
			number = ("{0}{1}".format(chaining, tknum), self.track.line)
		else:
			parity = self.track.up_parity if self.direction == Direction.UP \
				else self.track.down_parity
			chaining = self.position

			if parity == Parity.ODDEVEN:
				chaining = round(chaining)
			elif parity == Parity.EVEN:
				chaining = 2 * round(chaining / 2)
			else:
				chaining = 1 + 2 * round((chaining - 1) / 2)

			if self.track.style == NumberingStyle.NEW_YORK_B:
				tknum = self.track.up_number if self.direction == Direction.UP else self.track.down_number
				number = ("{0}{1}".format(self.track.line, tknum), chaining)
			else:
				prefix = self.track.up_prefix if self.direction == Direction.UP \
					else self.track.down_prefix
				number = (prefix, chaining)

		return (number, suffix)

	@property
	def number(self):
		parts = self._number_parts

		number, suffix = None, None
		if len(parts) == 1:
			number = parts[0]
		elif len(parts) == 2:
			number, suffix = parts

		left = ""
		if number:
			if self.track.style == NumberingStyle.TORONTO:
				left = "{0}{1}".format(*number)
			else:
				left = "{0}-{1}".format(*number)

		right = ""
		if self.lever:
			if self.track.style == NumberingStyle.NEW_YORK_A:
				suffix = (suffix[0], suffix[1].lower())
				right = "{0}L{1}X".format(*suffix)
			else:
				right = "X{1}{0}".format(*suffix)

		return left + " " + right if left and right else left + right

	def __repr__(self):
		return "<Signal {0}>".format(self.number) if self.number else "<Signal>"

	def __str__(self):
		return self.number

	@property
	def control_length(self):
		return self._control_length.length

	@property
	def state(self):
		return self._state

	@state.setter
	def state(self, state):
		if self._state != state:
			self._state = state
			for func in self._state_observers:
				func(self)

	def on_change_state(self, func):
		self._state_observers.append(func)

	def on_change_state_remove(self, func):
		self._state_observers.remove(func)

	@property
	def locked(self):
		return self._locked

	@locked.setter
	def locked(self, val):
		v = bool(val)
		if self._locked != v:
			self._locked = v
			for func in self._locked_observers:
				func(self)

	def on_change_locked(self, func):
		self._locked_observers.append(func)

	def on_change_locked_remove(self, func):
		self._locked_observers.remove(func)

	@property
	def manual(self):
		return self._manual

	@manual.setter
	def manual(self, val):
		v = bool(val)
		if self._manual != v:
			self._manual = v
			for func in self._manual_observers:
				func(self)

	def on_change_manual(self, func):
		self._manual_observers.append(func)

	def on_change_manual_remove(self, func):
		self._manual_observers.remove(func)

	def attach(self, track, position, direction):
		"""
		Attach the signal to a track at the specified position, such that
		the signal can be seen while moving in the specified direction.
		"""

		if not isinstance(track, Track) and not isinstance(position, int) \
			and not isinstance(position, float):
			raise SignalError("invalid parameters")

		position = float(position)

		signals = None
		if direction == Direction.UP:
			signals = track.up_signals
		elif direction == Direction.DOWN:
			signals = track.down_signals
		else:
			raise SignalError("invalid direction")

		if position not in track:
			raise SignalError("no such position on track")
		if position in signals:
			raise SignalError("existing signal at position")

		# The track must be on the same network as the lever.
		if self.lever is not None and track.network != self.lever.network:
			raise SignalError("mismatch between lever and track")

		signals[position] = self
		self.track = track
		self.position = position
		self.direction = direction
		self.network = track.network
		self.network.signals.add(self)

	async def switch(self, state):
		self.reset()

	@property
	def clear(self):
		return self.state == SignalState.CLEAR and (self.lever.reverse if self.lever else True)

	@property
	def clearing(self):
		return self.state == SignalState.CLEARING or self.grade_time or self.station_time

	@property
	def not_clear(self):
		return self.state == SignalState.NOT_CLEAR or \
			(self.state == SignalState.CLEAR and self.lever and self.lever.normal)

	def can_clear(self):
		"""
		Return whether the control length
		of this signal is clear.
		"""
		return all(not s.occupied for s in self.control_length)

	def can_clear_ST(self):
		"""
		Return whether the control length of this signal
		is clear if station time is active.
		"""
		return False if self.station_time is None else \
			all(not s.occupied for s in self.control_length[:-1])

	def _clear(self):
		if self.locked:
			return
		self.state = SignalState.CLEAR

	def _not_clear(self):
		if self.locked:
			return
		self.state = SignalState.NOT_CLEAR

	def reset(self):
		for sec in self.control_length:
			self.on_occupancy_change(sec)

	def on_occupancy_change(self, sec):
		if sec not in self.control_length:
			return

		if not sec.occupied and not sec.locked:
			self.locked = False

		if self.can_clear() and self.grade_time is None:
			self._clear()
			return

		self._not_clear()

		if self.grade_time and not self.can_clear():
			self.grade_time.cancel()
		elif self.station_time and not self.can_clear_ST():
			self.station_time.cancel()

	def on_GT_section_change(self, sec):
		if self.grade_time is None:
			return
		if sec.occupied and self.can_clear():
			self.grade_time.cancel_first()
			self.grade_time.start(self._clear)

	def on_2GT_section_change(self, sec):
		if self.grade_time is None:
			return
		if sec.occupied and self.can_clear():
			self.grade_time.start_first(self._clear)

	def on_ST_section_change(self, sec):
		if self.station_time is None or sec != self.station_time.timed_section:
			return
		if sec.occupied and self.can_clear_ST():
			self.station_time.start(self._clear)

	@property
	def next_signal(self):
		return self.track.next_signal(self.position, self.direction)

	@property
	def prev_signal(self):
		return self.track.prev_signal(self.position, self.direction)
