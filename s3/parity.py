from enum import Enum

class Parity(Enum):
	"""
	The parity of the chaining numbers on a section of track.
	In the Toronto convention, tracks are typically numbered
	only odd or only even in one direction, whereas in the
	New York convention tracks can be numbered with both odd
	and even chaining numbers.
	"""

	ODD = 1
	EVEN = 2
	ODDEVEN = 3
