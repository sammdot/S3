from .logger import Logger
from ._parser import TokenType, NodeType
from .parser import Token, Parser, ParseTreeNode
from .switch import Switch


class LayoutParser(Parser):

	def __init__(self, data, filename, warn=False):
		Parser.__init__(self, data, filename, warn)

	# BEGIN PARSER

	def _block(self):

		# block -> NEWLINE* statement ( NEWLINE+ statement )*
		# almost identical to Parser.block, but without '}' tokens
		stmts = []
		while self.match(TokenType.NEWLINE):
			self.next()
		stmts.append(self._statement())
		while self.token and not self.match(TokenType.EOF):
			if not self.accept(TokenType.NEWLINE):
				self.syntax_error("unexpected symbol", self.token)
			while self.match(TokenType.NEWLINE):
				self.next()
			stmt = self._statement()
			if stmt is None:
				break
			stmts.append(stmt)
		return stmts

	def _statement(self):

		opening = self.token

		# statement -> NETWORK IDENTIFIER
		if self.accept(TokenType.NETWORK):
			token = self.accept(TokenType.IDENTIFIER)
			if token:
				return ParseTreeNode(NodeType.NETWORK, token.value, start=opening)
			else:
				self.syntax_error("expected network name", self.token)

		# statement -> VIEWPORT NUMBER NUMBER NUMBER NUMBER
		elif self.accept(TokenType.VIEWPORT):
			x1 = self.accept(TokenType.NUMBER)
			if not x1:
				self.error("expected number", x1)
			y1 = self.accept(TokenType.NUMBER)
			if not y1:
				self.error("expected number", y1)
			x2 = self.accept(TokenType.NUMBER)
			if not x2:
				self.error("expected number", x2)
			y2 = self.accept(TokenType.NUMBER)
			if not y2:
				self.error("expected number", y2)
			point1 = (float(x1.value), float(y1.value))
			point2 = (float(x2.value), float(y2.value))
			return ParseTreeNode(NodeType.VIEWPORT, point1, point2, start=opening)

		# statement -> IDENTIFIER AT NUMBER token+
		# statement -> IDENTIFIER SLASH ( NUMBER | LEVER_NUMBER ) ( LEFT | RIGHT ) NUMBER
		# statement -> IDENTIFIER NUMBER NUMBER NUMBER NUMBER
		elif self.match(TokenType.IDENTIFIER):
			id = self.accept(TokenType.IDENTIFIER)

			if self.accept(TokenType.AT):
				offset = self.accept(TokenType.NUMBER)
				if not offset:
					self.error("expected number", self.token)
					return
				path = []
				while self.token and not self.match(TokenType.NEWLINE, TokenType.EOF):
					if self.token.value is not None:
						path.append(self.token.value)
					token = self.next()
				return ParseTreeNode(NodeType.TRACK, id.value,
					float(offset.value), " ".join(path), start=opening)

			elif self.accept(TokenType.SLASH):
				lever = self.accept(TokenType.NUMBER, TokenType.LEVER_NUMBER)
				if not lever:
					self.error("expected lever number", self.token)
					return
				side = self.accept(TokenType.LEFT, TokenType.RIGHT)
				if not side:
					self.error("expected 'left' or 'right'", self.token)
					return
				offset = self.accept(TokenType.NUMBER)
				if not offset:
					self.error("expected number", self.token)
					return
				offsetval = float(offset.value)
				if not 0 <= offsetval <= 1:
					self.error("expected number between 0 and 1", offset)
					return
				return ParseTreeNode(NodeType.SWITCH, id.value, lever.value,
					"L" if side.type == TokenType.LEFT else "R", offsetval, start=opening)

			elif self.match(TokenType.NUMBER):
				x1 = self.accept(TokenType.NUMBER)
				y1 = self.accept(TokenType.NUMBER)
				if not y1:
					self.error("expected number", self.token)
					return
				x2 = self.accept(TokenType.NUMBER)
				if not x2:
					self.error("expected number", self.token)
					return
				y2 = self.accept(TokenType.NUMBER)
				if not y2:
					self.error("expected number", self.token)
					return
				point1 = (float(x1.value), float(y1.value))
				point2 = (float(x2.value), float(y2.value))
				return ParseTreeNode(NodeType.INTERLOCKING, id.value, point1, point2, start=opening)

			else:
				self.error("unexpected symbol", self.token)

		elif self.match(TokenType.EOF):
			return

		else:
			self.error("unexpected symbol", self.token)


class LayoutReader(Logger):

	def __init__(self, network, data, filename, warn=False):
		Logger.__init__(self, filename, warn)

		self.network = network
		self.parser = LayoutParser(data, filename, warn=warn)
		self.lines = self.parser.lines
		self.tree = self.parser.parse()

	def create_layouts(self):
		layouts = {}
		_layout_locations = {}

		for stmt in self.tree:
			if stmt.type == NodeType.NETWORK:
				self._match_network(stmt)
			elif stmt.type == NodeType.VIEWPORT:
				layouts["_viewport"] = list(stmt.items)
			else:
				layout = self._create(stmt)
				if layout is None:
					continue

				key, val = layout
				if key in layouts:
					self.error("object already defined", stmt)
					self.note("object first defined here", _layout_locations[key])
					return
				layouts[key] = val
				_layout_locations[key] = stmt

		if self.errors != 0:
			return None
		return layouts

	def _match_network(self, stmt):
		name = stmt.items[0]
		if name != self.network.id:
			self.error("network name does not match", stmt)

	def _create(self, stmt):
		if stmt.type == NodeType.INTERLOCKING:
			name = stmt.items[0]
			intg = self.network.interlocking(name)
			if intg is None:
				self.error("no such interlocking", stmt)
				return None

			return intg, list(stmt.items[1:])

		elif stmt.type == NodeType.TRACK:
			track, offset, path = stmt.items
			sec = self.network.track_section("{0}@{1}".format(track, offset))
			if sec is None:
				self.error("no such track section", stmt)
				return None

			return sec, path

		elif stmt.type == NodeType.SWITCH:
			intg, lever, side, offset = stmt.items
			switch = self.network.lever_object("{0}/{1}".format(intg, lever))
			if switch is None or not isinstance(switch, Switch):
				self.error("no such switch", stmt)
				return None

			return switch, (side, offset)
