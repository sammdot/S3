from ._parser import TokenType, NodeType
from .track import NumberingStyle
from .direction import Direction
from .station import PlatformSide
from .parity import Parity
from .logger import Logger
import re

class Token:

	def __init__(self, type, offset, value=None):
		self.type = type
		self.start, self.end, self.length = offset
		self.value = value

	def __repr__(self):
		return "<{0} '{1}'>".format(self.type.name, self.value) \
			if self.value else "<{0}>".format(self.type.name)

class ParseTreeNode:

	def __init__(self, type, *items, start=None):
		self.type = type
		self.items = list(items)

		self.start = start.start

	def __repr__(self):
		return "<{0}({1})>".format(self.type.name.lower(), ", ".join(map(str, self.items)))

class ParseError(Exception):
	pass

class Parser(Logger):

	def __init__(self, data, filename, warn=False):
		Logger.__init__(self, filename, warn)

		self.data = data
		self.lines = [None] + data.splitlines()
		self.stack = []
		self.input = self.tokenize()
		self.token = None

		self._brace_count = 0
		self._paren_count = 0

		self.context_stack = [None]
		self.numbering = None

	def next(self):
		try:
			self.token = next(self.input)
			if self.match(TokenType.EOF):
				self.on_eof()
		except StopIteration:
			self.token = None

	def match(self, *types):
		return self.token.type in types

	def accept(self, *types):
		if self.token is None:
			return None
		if self.match(*types):
			t = self.token
			self.next()
			return t
		return None

	def parse(self):
		try:
			tree = self._program()
			if self.errors:
				return None
			return tree
		except:
			return None

	def on_eof(self):
		eof = self.token
		if self.brace_count:
			self.syntax_error("unbalanced braces", eof)
		elif self.paren_count:
			self.syntax_error("unbalanced parentheses", eof)

	@property
	def brace_count(self):
		return self._brace_count

	@brace_count.setter
	def brace_count(self, val):
		self._brace_count = val
		if val < 0:
			self.syntax_error("closing brace without matching opening brace", self.token)

	@property
	def paren_count(self):
		return self._paren_count

	@paren_count.setter
	def paren_count(self, val):
		self._paren_count = val
		if val < 0:
			self.syntax_error("closing parenthesis without matching opening parenthesis", self.token)

	# BEGIN PARSER

	def _program(self):

		# program -> block
		self.next()
		return self._block()

	def _block(self):

		# block -> NEWLINE* statement ( NEWLINE+ statement )*
		stmts = []
		while self.match(TokenType.NEWLINE):
			self.next()
		stmts.append(self._statement())
		while self.token and not self.match(TokenType.CLOSE_BRACE, TokenType.EOF):
			if not self.accept(TokenType.NEWLINE):
				self.syntax_error("unexpected symbol", self.token)
			while self.match(TokenType.NEWLINE):
				self.next()
			stmt = self._statement()
			if stmt is None:
				break
			stmts.append(stmt)
		return stmts

	def _statement(self):

		opening = self.token

		# statement -> NETWORK IDENTIFIER
		if self.accept(TokenType.NETWORK):
			token = self.accept(TokenType.IDENTIFIER)
			if token:
				if self.context_stack[-1]:
					self.error("'network' statement not allowed here", opening)
				return ParseTreeNode(NodeType.NETWORK, token.value, start=opening)
			else:
				self.syntax_error("expected network name", self.token)

		# statement -> NUMBERING ( NEW_YORK_A | NEW_YORK_B | TORONTO )
		elif self.accept(TokenType.NUMBERING):
			token = self.accept(TokenType.NEW_YORK_A, TokenType.NEW_YORK_B, TokenType.TORONTO)
			if self.context_stack[-1]:
				self.error("'numbering' statement not allowed here", opening)
			if token:
				if token.type == TokenType.NEW_YORK_A:
					self.numbering = NumberingStyle.NEW_YORK_A
				elif token.type == TokenType.NEW_YORK_B:
					self.numbering = NumberingStyle.NEW_YORK_B
				elif token.type == TokenType.TORONTO:
					self.numbering = NumberingStyle.TORONTO
				return ParseTreeNode(NodeType.NUMBERING, self.numbering, start=opening)
			else:
				self.syntax_error("expected numbering type", self.numbering)

		# statement -> TRACK IDENTIFIER suite
		# statement -> TRACK NUMBER_ name name?
		elif self.accept(TokenType.TRACK):
			if self.numbering is None:
				self.syntax_error("'numbering' statement required before 'track'", opening)
			if self.accept(TokenType.NUMBER_):
				name1 = self._name()
				if not name1:
					self.syntax_error("expected track number", self.token)
				name2 = self._name()
				return ParseTreeNode(NodeType.TRACK_NUMBER, name.value, name2.value, start=opening)
			else:
				token = self.accept(TokenType.IDENTIFIER)
				if not token:
					self.syntax_error("expected track name", self.token)
				self.context_stack.append(NodeType.TRACK)
				block = self._suite()
				self.context_stack.pop()
				return ParseTreeNode(NodeType.TRACK, token.value, block, start=opening)

		# statement -> INTERLOCKING IDENTIFIER suite
		elif self.accept(TokenType.INTERLOCKING):
			name = self.accept(TokenType.IDENTIFIER)
			if not name:
				self.syntax_error("expected interlocking name", self.token)
			if self.context_stack[-1]:
				self.error("'interlocking' statement not allowed here", opening)
			self.context_stack.append(NodeType.INTERLOCKING)
			block = self._suite()
			self.context_stack.pop()
			return ParseTreeNode(NodeType.INTERLOCKING, name.value, block, start=opening)

		# statement -> SIGNAL trackoffset direction suite
		elif self.accept(TokenType.SIGNAL):
			offset = self._trackoffset()
			direction = self.accept(TokenType.UP, TokenType.DOWN)
			if not direction:
				self.syntax_error("expected direction", self.token)
			if self.context_stack[-1]:
				self.error("'signal' statement not allowed here", opening)
			self.context_stack.append(NodeType.SIGNAL)
			block = self._suite()
			self.context_stack.pop()
			dir = Direction.UP if direction.type == TokenType.UP else Direction.DOWN
			return ParseTreeNode(NodeType.SIGNAL, offset, dir, block, start=opening)

		# statement -> SWITCH trackoffset direction suite
		elif self.accept(TokenType.SWITCH):
			offset = self._trackoffset()
			direction = self.accept(TokenType.UP, TokenType.DOWN)
			if not direction:
				self.syntax_error("expected direction", self.token)
			if self.context_stack[-1]:
				self.error("'switch' statement not allowed here", opening)
			self.context_stack.append(NodeType.SWITCH)
			block = self._suite()
			self.context_stack.pop()
			dir = Direction.UP if direction.type == TokenType.UP else Direction.DOWN
			return ParseTreeNode(NodeType.SWITCH, offset, dir, block, start=opening)

		# statement -> PLATFORM ( NUMBER | IDENTIFIER | STRING )
		#              OPEN_PAREN trackoffset ( COMMA trackoffset )* CLOSE_PAREN
		#              ( LEFT | RIGHT )
		elif self.accept(TokenType.PLATFORM):
			if self.context_stack[-1] != NodeType.STATION:
				self.error("'platform' statement not allowed here", opening)
			name = self.accept(TokenType.NUMBER, TokenType.IDENTIFIER, TokenType.STRING)
			if not name:
				self.syntax_error("expected platform name", self.token)
			if not self.accept(TokenType.OPEN_PAREN):
				self.syntax_error("expected '('", self.token)
			self.paren_count = self.paren_count + 1
			offsets = [self._trackoffset()]
			while not self.accept(TokenType.CLOSE_PAREN):
				if not self.accept(TokenType.COMMA):
					self.syntax_error("expected ',' or ')'", self.token)
				offsets.append(self._trackoffset())
			self.paren_count = self.paren_count - 1
			length = ParseTreeNode(NodeType.PLATFORM_LENGTH, *offsets, start=opening)
			side = self.accept(TokenType.LEFT, TokenType.RIGHT)
			if not side:
				self.syntax_error("expected platform side", self.token)
			side_ = PlatformSide.LEFT if side.type == TokenType.LEFT else PlatformSide.RIGHT
			return ParseTreeNode(NodeType.PLATFORM, name.value, length, side_, start=opening)

		# statement -> TURNOUT IDENTIFIER ( SLASH ( NUMBER | LEVER_NUMBER ) | UP | DOWN )
		elif self.accept(TokenType.TURNOUT):
			if self.context_stack[-1] != NodeType.SWITCH:
				self.error("'turnout' statement not allowed here", opening)
			name = self.accept(TokenType.IDENTIFIER)
			if not name:
				self.syntax_error("expected track or interlocking name", self.token)
			if self.accept(TokenType.SLASH):
				number = self.accept(TokenType.NUMBER, TokenType.LEVER_NUMBER)
				if not number:
					self.syntax_error("expected lever number", self.token)
				return ParseTreeNode(NodeType.TURNOUT_SWITCH, name.value, number.value, start=opening)
			else:
				direction = self.accept(TokenType.UP, TokenType.DOWN)
				if not direction:
					self.syntax_error("expected '/' or direction", self.token)
				dir = Direction.UP if direction.type == TokenType.UP else Direction.DOWN
				return ParseTreeNode(NodeType.TURNOUT_TRACK, name.value, dir, start=opening)

		# statement -> ATTACH IDENTIFIER IDENTIFIER ( UP | DOWN )
		elif self.accept(TokenType.ATTACH):
			if self.context_stack[-1]:
				self.error("'attach' statement not allowed here", opening)
			trk1 = self.accept(TokenType.IDENTIFIER)
			if not trk1:
				self.syntax_error("expected track name", self.token)
			trk2 = self.accept(TokenType.IDENTIFIER)
			if not trk2:
				self.syntax_error("expected track name", self.token)
			direction = self.accept(TokenType.UP, TokenType.DOWN)
			if not direction:
				self.syntax_error("expected direction", self.token)
			dir = Direction.UP if direction.type == TokenType.UP else Direction.DOWN
			return ParseTreeNode(NodeType.ATTACH, trk1.value, trk2.value, dir, start=opening)

		# statement -> ( UP | DOWN ) offset ( ODD | EVEN )?
		elif self.match(TokenType.UP, TokenType.DOWN):
			direction = self.accept(TokenType.UP, TokenType.DOWN)
			if self.context_stack[-1] != NodeType.TRACK:
				dir = "up" if direction.type == TokenType.UP else "down"
				self.error("'{0}' statement not allowed here".format(dir), opening)
			offset = self._offset()
			parity = self.accept(TokenType.ODD, TokenType.EVEN)
			dir = Direction.UP if direction.type == TokenType.UP else Direction.DOWN
			par = Parity.ODD if parity.type == TokenType.ODD else \
					Parity.EVEN if parity.type == TokenType.EVEN else Parity.ODDEVEN
			return ParseTreeNode(NodeType.TRACK_DIRECTION, dir, offset, par, start=opening)

		# statement -> PREFIX IDENTIFIER IDENTIFIER
		elif self.accept(TokenType.PREFIX):
			if self.context_stack[-1] != NodeType.TRACK:
				self.error("'prefix' statement not allowed here", opening)
			if self.numbering != NumberingStyle.TORONTO:
				self.error("'prefix' statement not allowed in New York numbering", opening)
			up_prefix = self.accept(TokenType.IDENTIFIER)
			if not up_prefix:
				self.syntax_error("expected track prefix", self.token)
			down_prefix = self.accept(TokenType.IDENTIFIER)
			if not down_prefix:
				self.syntax_error("expected track prefix", self.token)
			return ParseTreeNode(NodeType.PREFIX, up_prefix.value, down_prefix.value, start=opening)

		# statement -> CHAINING LINE IDENTIFIER
		elif self.accept(TokenType.CHAINING):
			if self.accept(TokenType.LINE):
				if self.context_stack[-1] != NodeType.TRACK:
					self.error("'chaining line' statement not allowed here", opening)
				if self.numbering == NumberingStyle.TORONTO:
					self.error("'chaining line' statement not allowed in Toronto numbering", opening)
				name = self.accept(TokenType.IDENTIFIER)
				if not name:
					self.syntax_error("expected chaining line name", self.token)
				return ParseTreeNode(NodeType.CHAINING_LINE, name.value, start=opening)
			else:
				self.syntax_error("expected 'line'", self.token)

		# statement -> JOINTS offset+
		elif self.accept(TokenType.JOINTS):
			if self.context_stack[-1] != NodeType.TRACK:
				self.error("'joints' statement not allowed here", opening)
			joints = []
			while not self.match(TokenType.NEWLINE, TokenType.CLOSE_BRACE):
				joints.append(self._offset())
			return ParseTreeNode(NodeType.JOINTS, joints, start=opening)

		# statement -> LEVER levernum
		# statement -> LEVER NUMBER suite
		elif self.accept(TokenType.LEVER):
			if self.match(TokenType.NUMBER):
				if self.context_stack[-1] != NodeType.INTERLOCKING:
					self.error("'lever' statement not allowed here", opening)
				number = self.accept(TokenType.NUMBER)
				self.context_stack.append(NodeType.LEVER)
				block = self._suite()
				self.context_stack.pop()
				return ParseTreeNode(NodeType.LEVER, number.value, block, start=opening)
			else:
				if not self.match(TokenType.IDENTIFIER):
					self.syntax_error("expected lever number", self.token)
				if self.context_stack[-1] not in [NodeType.SIGNAL, NodeType.SWITCH]:
					self.error("'lever' statement not allowed here", opening)
				return self._levernum()

		# statement -> APPROACH LEVER levernum
		elif self.accept(TokenType.APPROACH):
			tok = self.accept(TokenType.LEVER)
			if tok:
				if self.context_stack[-1] != NodeType.SIGNAL:
					self.error("'approach lever' statement not allowed here", opening)
				lever = self._levernum()
				return ParseTreeNode(NodeType.APPROACH_LEVER, *lever.items, start=opening)
			else:
				self.syntax_error("expected 'lever'", self.token)

		# statement -> CONTROL LENGTH suite
		elif self.accept(TokenType.CONTROL):
			tok = self.accept(TokenType.LENGTH)
			if tok:
				if self.context_stack[-1] != NodeType.SIGNAL:
					self.error("'control length' statement not allowed here", opening)
				self.context_stack.append(NodeType.CONTROL_LENGTH)
				block = self._suite()
				self.context_stack.pop()
				return ParseTreeNode(NodeType.CONTROL_LENGTH, block, start=opening)
			else:
				self.syntax_error("expected 'length'", self.token)

		# statement -> GRADE TIME suite
		elif self.accept(TokenType.GRADE):
			tok = self.accept(TokenType.TIME)
			if tok:
				if self.context_stack[-1] != NodeType.SIGNAL:
					self.error("'grade time' statement not allowed here", opening)
				self.context_stack.append(NodeType.GRADE_TIME)
				block = self._suite()
				self.context_stack.pop()
				return ParseTreeNode(NodeType.GRADE_TIME, block, start=opening)
			else:
				self.syntax_error("expected 'time'", self.token)

		# statement -> STATION TIME suite
		# statement -> STATION IDENTIFIER suite
		elif self.accept(TokenType.STATION):
			tok = self.accept(TokenType.TIME)
			if tok:
				if self.context_stack[-1] != NodeType.SIGNAL:
					self.error("'station time' statement not allowed here", opening)
				self.context_stack.append(NodeType.STATION_TIME)
				block = self._suite()
				self.context_stack.pop()
				return ParseTreeNode(NodeType.STATION_TIME, block, start=opening)
			else:
				name = self.accept(TokenType.IDENTIFIER)
				if not name:
					self.syntax_error("expected 'time' or station name", self.token)
				if self.context_stack[-1]:
					self.error("'station' statement not allowed here", opening)
				self.context_stack.append(NodeType.STATION)
				block = self._suite()
				self.context_stack.pop()
				return ParseTreeNode(NodeType.STATION, name.value, block, start=opening)

		# statement -> TIMED SECTION suite
		elif self.accept(TokenType.TIMED):
			tok = self.accept(TokenType.SECTION)
			if tok:
				if self.context_stack[-1] not in [NodeType.GRADE_TIME, NodeType.STATION_TIME]:
					self.error("'timed section' statement not allowed here", opening)
				self.context_stack.append(NodeType.TIMED_SECTION)
				block = self._suite()
				self.context_stack.pop()
				return ParseTreeNode(NodeType.TIMED_SECTION, block, start=opening)
			else:
				self.syntax_error("expected 'section'", self.token)

		# statement -> TIME NUMBER
		elif self.accept(TokenType.TIME):
			num = self.accept(TokenType.NUMBER)
			if not num:
				self.syntax_error("expected number", self.token)
			if self.context_stack[-1] not in [NodeType.GRADE_TIME, NodeType.STATION_TIME]:
				self.error("'time' statement not allowed here", opening)
			return ParseTreeNode(NodeType.TIME, float(num.value), start=opening)

		# statement -> FIRST TIME suite
		# statement -> FIRST TIMED SECTION suite
		elif self.accept(TokenType.FIRST):
			if self.accept(TokenType.TIME):
				num = self.accept(TokenType.NUMBER)
				if not num:
					self.syntax_error("expected number", self.token)
				if self.context_stack[-1] not in [NodeType.GRADE_TIME, NodeType.STATION_TIME]:
					self.error("'first time' statement not allowed here", opening)
				return ParseTreeNode(NodeType.FIRST_TIME, float(num.value), start=opening)
			elif self.accept(TokenType.TIMED):
				tok = self.accept(TokenType.SECTION)
				if tok:
					if self.context_stack[-1] not in [NodeType.GRADE_TIME, NodeType.STATION_TIME]:
						self.error("'first timed section' statement not allowed here", opening)
					self.context_stack.append(NodeType.TIMED_SECTION)
					block = self._suite()
					self.context_stack.pop()
					return ParseTreeNode(NodeType.FIRST_TIMED_SECTION, block, start=opening)
				else:
					self.syntax_error("expected 'section'", self.token)
			else:
				self.syntax_error("expected 'time' or 'timed section'", self.token)

		# statement -> SECOND TIME suite
		# statement -> SECOND TIMED SECTION suite
		elif self.accept(TokenType.SECOND):
			if self.accept(TokenType.TIME):
				num = self.accept(TokenType.NUMBER)
				if not num:
					self.syntax_error("expected number", self.token)
				if self.context_stack[-1] not in [NodeType.GRADE_TIME, NodeType.STATION_TIME]:
					self.error("'second time' statement not allowed here", opening)
				return ParseTreeNode(NodeType.SECOND_TIME, float(num.value), start=opening)
			elif self.accept(TokenType.TIMED):
				tok = self.accept(TokenType.SECTION)
				if tok:
					if self.context_stack[-1] not in [NodeType.GRADE_TIME, NodeType.STATION_TIME]:
						self.error("'second timed section' statement not allowed here", opening)
					self.context_stack.append(NodeType.TIMED_SECTION)
					block = self._suite()
					self.context_stack.pop()
					return ParseTreeNode(NodeType.SECOND_TIMED_SECTION, block, start=opening)
				else:
					self.syntax_error("expected 'section'", self.token)
			else:
				self.syntax_error("expected 'time' or 'timed section'", self.token)

		# statement -> NORMAL levercond
		elif self.accept(TokenType.NORMAL):
			cond = self._levercond()
			if self.context_stack[-1] != NodeType.LEVER:
				self.error("'normal' statement not allowed here", opening)
			return ParseTreeNode(NodeType.NORMAL, cond, start=opening)

		# statement -> REVERSE levercond
		elif self.accept(TokenType.REVERSE):
			cond = self._levercond()
			if self.context_stack[-1] != NodeType.LEVER:
				self.error("'reverse' statement not allowed here", opening)
			return ParseTreeNode(NodeType.REVERSE, cond, start=opening)

		# EOF: get out of here
		elif self.accept(TokenType.EOF):
			return

		# statement -> levercond EQUAL trackoffset ( COMMA trackoffset )*
		else:
			if self.match(TokenType.IDENTIFIER, TokenType.LEVER_CONDITION, TokenType.OPEN_PAREN):
				cond = self._levercond()
				tok = self.accept(TokenType.EQUAL)
				if not tok:
					self.syntax_error("expected '='", self.token)
				offsets = [self._trackoffset()]
				while not self.match(TokenType.NEWLINE, TokenType.CLOSE_BRACE):
					if not self.accept(TokenType.COMMA):
						self.syntax_error("expected ','", self.token)
					offsets.append(self._trackoffset())
				length = ParseTreeNode(NodeType.CONTROL_LENGTH, *offsets, start=offsets[0])
				if self.context_stack[-1] not in [NodeType.CONTROL_LENGTH, NodeType.TIMED_SECTION]:
					self.error("lever condition statement not allowed here", opening)
				return ParseTreeNode(NodeType.LENGTH_CONDITION, cond, length, start=opening)
			else:
				self.syntax_error("unexpected symbol", self.token)

	def _suite(self):

		# suite -> NEWLINE* OPEN_BRACE NEWLINE*
		#          ( statement ( NEWLINE+ statement )* NEWLINE* )? CLOSE_BRACE
		while self.match(TokenType.NEWLINE):
			self.next()
		opening = None
		if self.match(TokenType.OPEN_BRACE):
			opening = self.token
			self.accept(TokenType.OPEN_BRACE)
			self.brace_count = self.brace_count + 1
		else:
			self.syntax_error("expected '{'", self.token)
		while self.match(TokenType.NEWLINE):
			self.next()
		if self.match(TokenType.CLOSE_BRACE):
			self.warning("empty block", opening)
			self.accept(TokenType.CLOSE_BRACE)
			self.brace_count = self.brace_count - 1
			return []
		stmts = [self._statement()]
		while self.token and not self.match(TokenType.CLOSE_BRACE):
			if not self.accept(TokenType.NEWLINE):
				self.syntax_error("expected newline before next statement", self.token)
			while self.match(TokenType.NEWLINE):
				self.next()
			if self.match(TokenType.CLOSE_BRACE):
				break
			stmt = self._statement()
			if stmt is None:
				break
			stmts.append(stmt)
		if self.match(TokenType.CLOSE_BRACE):
			if len(stmts) == 0:
				self.warning("empty block", opening)
			self.accept(TokenType.CLOSE_BRACE)
			self.brace_count = self.brace_count - 1
		else:
			self.syntax_error("expected '}'", self.token)
		return stmts

	def _name(self):

		# name -> NUMBER | IDENTIFIER | STRING
		return self.accept(TokenType.NUMBER, TokenType.IDENTIFIER, TokenType.STRING)

	def _offset(self):

		# offset -> AT NUMBER
		if self.accept(TokenType.AT):
			number = self.accept(TokenType.NUMBER)
			if not number:
				self.syntax_error("expected track offset", self.token)
			return float(number.value)
		else:
			self.syntax_error("expected '@'", self.token)

	def _trackoffset(self):

		# trackoffset -> IDENTIFIER offset
		name = self.accept(TokenType.IDENTIFIER)
		if not name:
			self.syntax_error("expected track name", self.token)
		offset = self._offset()
		return ParseTreeNode(NodeType.TRACK_OFFSET, name.value, offset, start=name)

	def _levernum(self):

		# levernum -> IDENTIFIER SLASH ( NUMBER | LEVER_NUMBER )
		intg = self.accept(TokenType.IDENTIFIER)
		if not intg:
			self.syntax_error("expected interlocking name", self.token)
		if not self.accept(TokenType.SLASH):
			self.syntax_error("expected '/'", self.token)
		num = self.accept(TokenType.NUMBER, TokenType.LEVER_NUMBER)
		if not num:
			self.syntax_error("expected lever number", self.token)
		return ParseTreeNode(NodeType.LEVER_NUMBER, intg.value, num.value, start=intg)

	def _levercond(self):

		# levercond -> lever_cond ( ( AND | OR ) levercond )*
		# levercond -> OPEN_PAREN levercond CLOSE_PAREN ( ( AND | OR ) levercond )*
		tok = self.accept(TokenType.OPEN_PAREN)
		if tok:
			self.paren_count = self.paren_count + 1
			conds = [self._levercond()]
			if not self.accept(TokenType.CLOSE_PAREN):
				self.syntax_error("expected ')'", self.token)
			self.paren_count = self.paren_count - 1
			while self.match(TokenType.AND, TokenType.OR):
				conds.append(self.accept(TokenType.AND, TokenType.OR))
				conds.append(self._levercond())
			return ParseTreeNode(NodeType.LEVER_CONDITION, *conds, start=tok)
		else:
			conds = [self._lever_cond()]
			while self.match(TokenType.AND, TokenType.OR):
				conds.append(self.accept(TokenType.AND, TokenType.OR))
				conds.append(self._levercond())
			return ParseTreeNode(NodeType.LEVER_CONDITION, *conds, start=conds[0])

	def _lever_cond(self):

		# lever_cond -> LEVER_CONDITION
		cond = self.accept(TokenType.LEVER_CONDITION)
		if not cond:
			self.syntax_error("expected lever condition", self.token)

		return ParseTreeNode(NodeType.LEVER_STATE_CONDITION, cond.value, start=cond)

	# END PARSER

	# BEGIN TOKENIZER

	TOKEN_RE = re.compile(r"""
		(?P<comment>    \#.*(?=\n)                 )
	|	(?P<punct>      \n|{|}|\(|\)|@|=|/|&|\||,  )
	|	(?P<levernum>   [0-9]\d*[a-z]              )
	|	(?P<levercond>  [0-9]\d*[NR]               )
	|	(?P<number>     (?:0|[1-9]\d*)(?:\.\d+)?   )
	|	(?P<word>       [\w-][\w\d-]*              )
	|	(?P<string>     "[^"]*?"|'[^']*?'          )
	|	\s+
	|	(?P<invalid>    .+?                        )""", re.X)

	PUNCT_TOKENS = {
		"\n": TokenType.NEWLINE,
		"{": TokenType.OPEN_BRACE,
		"}": TokenType.CLOSE_BRACE,
		"(": TokenType.OPEN_PAREN,
		")": TokenType.CLOSE_PAREN,
		"@": TokenType.AT,
		"=": TokenType.EQUAL,
		"/": TokenType.SLASH,
		"&": TokenType.AND,
		"|": TokenType.OR,
		",": TokenType.COMMA
	}

	KEYWORDS = {
		"network": TokenType.NETWORK,
		"interlocking": TokenType.INTERLOCKING,
		"lever": TokenType.LEVER,
		"signal": TokenType.SIGNAL,
		"switch": TokenType.SWITCH,
		"track": TokenType.TRACK,
		"station": TokenType.STATION,
		"platform": TokenType.PLATFORM,
		"numbering": TokenType.NUMBERING,
		"joints": TokenType.JOINTS,
		"number": TokenType.NUMBER_,
		"chaining": TokenType.CHAINING,
		"line": TokenType.LINE,
		"grade": TokenType.GRADE,
		"control": TokenType.CONTROL,
		"length": TokenType.LENGTH,
		"normal": TokenType.NORMAL,
		"reverse": TokenType.REVERSE,
		"approach": TokenType.APPROACH,
		"time": TokenType.TIME,
		"timed": TokenType.TIMED,
		"section": TokenType.SECTION,
		"first": TokenType.FIRST,
		"second": TokenType.SECOND,
		"turnout": TokenType.TURNOUT,
		"prefix": TokenType.PREFIX,
		"odd": TokenType.ODD,
		"even": TokenType.EVEN,
		"up": TokenType.UP,
		"down": TokenType.DOWN,
		"left": TokenType.LEFT,
		"right": TokenType.RIGHT,
		"NewYorkA": TokenType.NEW_YORK_A,
		"NewYorkB": TokenType.NEW_YORK_B,
		"Toronto": TokenType.TORONTO,
		"viewport": TokenType.VIEWPORT,
		"attach": TokenType.ATTACH
	}

	def tokenize(self):
		"""
		Return a generator sequence of tokens corresponding
		to the tokens in the provided string.
		"""

		lines = self.data.splitlines(True)
		offsets = []
		for i, line in enumerate(lines):
			if i == 0:
				offsets.append(0)
				continue
			offsets.append(offsets[i - 1] + len(lines[i - 1]))

		def offset_to_line_column(offset):
			for i, pair in enumerate(zip(offsets, offsets[1:])):
				left, right = pair
				if left <= offset < right:
					return i + 1, offset - left + 1
			return len(lines), offset - offsets[-1] + 1

		def offset(span):
			"""
			Convert the given span in the string to
			a line and column number and a length.
			"""
			return offset_to_line_column(span[0]), offset_to_line_column(span[1]), span[1] - span[0]

		for tok in Parser.TOKEN_RE.finditer(self.data):
			if tok.group("punct"):
				yield Token(Parser.PUNCT_TOKENS[tok.group("punct")], offset(tok.span("punct")))
			elif tok.group("number"):
				yield Token(TokenType.NUMBER, offset(tok.span("number")), tok.group("number"))
			elif tok.group("word"):
				word = tok.group("word")
				if word in Parser.KEYWORDS:
					yield Token(Parser.KEYWORDS[word], offset(tok.span("word")))
				else:
					yield Token(TokenType.IDENTIFIER, offset(tok.span("word")), word)
			elif tok.group("levernum"):
				yield Token(TokenType.LEVER_NUMBER, offset(tok.span("levernum")), tok.group("levernum"))
			elif tok.group("levercond"):
				yield Token(TokenType.LEVER_CONDITION, offset(tok.span("levercond")), tok.group("levercond"))
			elif tok.group("string"):
				yield Token(TokenType.STRING, offset(tok.span("string")), tok.group("string")[1:-1])
			elif tok.group("invalid"):
				self.syntax_error("unexpected symbol", Token(TokenType.INVALID, offset(tok.span("invalid")), tok.group("invalid")))

		yield Token(TokenType.EOF, offset((len(self.data) - 1, len(self.data))))

	# END TOKENIZER
