from .parser import Parser, Token, TokenType, NodeType, ParseError

from .network import TrackNetwork
from .interlocking import Interlocking
from .lever import Lever, LeverState, And, Or, LeverError
from .direction import Direction
from .parity import Parity
from .track import NumberingStyle, Track, TrackError
from .switch import Switch, SwitchError
from .station import Station, Platform, PlatformSide, StationError
from .control_length import ControlLength
from .timing import Timing, TimingType
from .signal import Signal, SignalError
from .logger import Logger

from collections import OrderedDict

class ReaderError(Exception):
	pass

class Reader(Logger):

	def __init__(self, data, filename, warn=False):
		Logger.__init__(self, filename, warn)

		self.parser = Parser(data, filename, warn=warn)
		self.lines = self.parser.lines
		self.tree = self.parser.parse()

		self.network = None
		self.numbering = None

		self._interlockings = {}
		self._interlocking_location = {}

		self._levers = {}
		self._lever_location = {}

		self._tracks = OrderedDict()
		self._track_location = {}

		self._switches = {}
		self._switch_location = {}
		self._switch_lever_location = {}
		self._turnout_location = {}

		self._stations = {}
		self._station_location = {}

		self._signals = {}
		self._signal_location = {}
		self._signal_lever_location = {}
		self._control_length_location = {}
		self._grade_time_location = {}
		self._station_time_location = {}

		self._to_attach = []
		self._switches_to_attach = []
		self._turnouts_to_attach = []
		self._platforms = []
		self._signals_to_attach = []

	def create_network(self):
		if self.tree is None:
			return None

		for stmt in self.tree:
			self._create_statement(stmt)
			if not self.network:
				self.error("no network defined", stmt)
				return None

		for att in self._to_attach:
			self._attach(*att)
		for sw in self._switches_to_attach:
			self._attach_switch(*sw)
		for to in self._turnouts_to_attach:
			self._attach_turnout(*to)
		for pf in self._platforms:
			self._create_platform(*pf)
		for sig in self._signals_to_attach:
			self._attach_signal(*sig)

		self._remove_dummy()

		if self.errors:
			return None
		return self.network

	def _create_statement(self, stmt):

		if stmt.type == NodeType.NETWORK:
			self.network = TrackNetwork(stmt.items[0])
			intg = self._interlockings["_dummy"] = Interlocking("_dummy", self.network)
			lever = Lever(0, intg)
			lever.state = LeverState.REVERSE
			return

		if not self.network:
			return

		type_to_function = {
			NodeType.NUMBERING: self._handle_numbering,
			NodeType.INTERLOCKING: self._handle_interlocking,
			NodeType.TRACK: self._handle_track,
			NodeType.SWITCH: self._handle_switch,
			NodeType.SIGNAL: self._handle_signal,
			NodeType.STATION: self._handle_station,
			NodeType.ATTACH: self._handle_attach
		}

		if stmt.type in type_to_function:
			type_to_function[stmt.type](stmt)
		else:
			self.error("invalid top-level object", stmt)

	def _handle_numbering(self, stmt):
		self.numbering = stmt.items[0]

	def _handle_interlocking(self, stmt):
		name, block = stmt.items

		if name in self._interlockings:
			self.error("interlocking already exists", stmt)
			self.note("interlocking first defind", self._interlocking_location[name])
			return

		intg = Interlocking(name, self.network)
		self._interlockings[name] = intg
		self._interlocking_location[name] = stmt
		self._levers[name] = {}
		self._lever_location[name] = {}

		# A dummy lever for unconditional lever 'conditions'.
		# 0R corresponds to True, 0N to False.
		l0 = Lever(0, intg)
		l0.state = LeverState.REVERSE
		self._levers[name][0] = l0
		self._lever_location[name][0] = stmt

		_levers = []

		for s in block:
			if s.type == NodeType.LEVER:
				lvr = self._handle_lever(intg, s, stmt)
				if lvr:
					_levers.append(lvr)
			else:
				self.error("invalid statement in interlocking", s)

		for l in _levers:
			if l is None:
				continue
			lever, normal, reverse = l
			normal = self._handle_lever_condition(normal, intg, stmt)
			reverse = self._handle_lever_condition(reverse, intg, stmt)
			if normal is not None:
				lever.normal_if(normal)
			if reverse is not None:
				lever.reverse_if(reverse)

	def _handle_lever(self, intg, stmt, parent):
		lever_id, conditions = stmt.items

		if int(lever_id) in self._levers[intg.id]:
			self.error("lever already exists", stmt)
			self.note("lever first defined", self._lever_location[intg.id][int(lever_id)])
			return

		lever = Lever(lever_id, intg)
		self._levers[intg.id][lever.id] = lever
		self._lever_location[intg.id][lever.id] = stmt

		normal = None
		reverse = None

		_normal = None
		_reverse = None

		for s in conditions:
			if s.type == NodeType.NORMAL:
				if normal:
					self.error("normal lever condition already defined", s)
					self.note("normal lever condition first defined", _normal)
					continue
				normal = s.items[0]
				_normal = s
			elif s.type == NodeType.REVERSE:
				if reverse:
					self.error("reverse lever condition already defined", s)
					self.note("reverse lever condition first defined", _reverse)
					continue
				reverse = s.items[0]
				_reverse = s

		return (lever, normal, reverse)

	def _handle_lever_condition(self, node, intg, parent):
		if node is None:
			return

		if len(node.items) == 1:
			nd = node.items[0]

			if isinstance(nd, str):
				lever, state = nd[:-1], nd[-1]

				levers = [l for l in intg.levers if l.id == int(lever)]
				if levers == []:
					self.error("no such lever", node)
					return
				lever = levers[0]

				return lever.normal if state == "N" else lever.reverse
			else:
				return self._handle_lever_condition(nd, intg, parent)

		left = self._handle_lever_condition(node.items[0], intg, parent)
		right = self._handle_lever_condition(node.items[2], intg, parent)

		if left is None or right is None:
			return

		if node.items[1].type == TokenType.AND:
			return left & right
		elif node.items[1].type == TokenType.OR:
			return left | right

	def _handle_track(self, stmt):
		name, block = stmt.items

		if name in self._tracks:
			self.error("track already defined", stmt)
			self.note("track first defined", self._track_location[name])
			return

		up_prefix = None
		down_prefix = None
		chaining_line = None
		up_number = None
		down_number = None
		up_parity = None
		down_parity = None
		up_end = None
		down_end = None
		joints = set()

		_up = None
		_down = None
		_prefix = None
		_chaining_line = None
		_track_number = None

		for s in block:
			if s.type == NodeType.TRACK_DIRECTION:
				direction, *items = s.items
				if direction == Direction.UP:
					if _up:
						self.error("duplicate 'up' statement", s)
						self.node("'up' statement first used", _up)
						continue
					up_end, up_parity = items
					_up = s
				elif direction == Direction.DOWN:
					if _down:
						self.error("duplicate 'down' statement", s)
						self.node("'down' statement first used", _down)
						continue
					down_end, down_parity = items
					_down = s
			elif s.type == NodeType.PREFIX:
				if _prefix:
					self.error("duplicate track prefixes", s)
					self.note("track prefixes first defined", _prefix)
					continue
				up_prefix, down_prefix = s.items
				_prefix = s
			elif s.type == NodeType.CHAINING_LINE:
				if _chaining_line:
					self.error("duplicate chaining line", s)
					self.note("chaining line first defined", _chaining_line)
					continue
				chaining_line = s.items[0]
				_chaining_line = s
			elif s.type == NodeType.TRACK_NUMBER:
				if _track_number:
					self.error("duplicate track number", s)
					self.note("track number first defined", _track_number)
					continue
				up_number = s.items[0]
				down_number = s.items[1] or up_number
				_track_number = s
			elif s.type == NodeType.JOINTS:
				jts = s.items[0]
				for jt in jts:
					joints.add(jt)

		try:
			track = Track(name, self.network, style=self.numbering, \
				line=chaining_line, up_number=up_number, down_number=down_number, \
				up_prefix=up_prefix, down_prefix=down_prefix, up=up_end, down=down_end, \
				up_parity=up_parity, down_parity=down_parity, joints=joints)
			self._tracks[name] = track
			self._track_location[name] = stmt
		except TrackError as e:
			self.error(e.args[0], stmt)

	def _handle_attach(self, stmt):
		self._to_attach.append((*stmt.items, stmt))

	def _attach(self, track1, track2, direction, stmt):
		if track1 not in self._tracks:
			self.error("no track named {0}".format(track1), stmt)
			return
		if track2 not in self._tracks:
			self.error("no track named {0}".format(track2), stmt)
			return

		trk1 = self._tracks[track1]
		trk2 = self._tracks[track2]
		try:
			trk1.attach(trk2, direction)
		except TrackError as e:
			self.error(e.args[0], stmt)

	def _handle_switch(self, stmt):
		trackoffset, direction, block = stmt.items
		track, offset = trackoffset.items

		tod = (track, offset, direction)

		if tod in self._switches:
			self.error("switch already defined", stmt)
			self.note("switch first defined", self._switch_location[tod])
			return
		self._switches[tod] = None
		self._switch_location[tod] = stmt

		lever = None
		turnout = None

		for s in block:
			if s.type == NodeType.LEVER_NUMBER:
				if lever is not None:
					self.error("lever already defined for this switch", s)
					self.note("lever first defined", self._switch_lever_location[tod])
					return
				self._switch_lever_location[tod] = s
				lever = tuple(s.items)
			elif s.type in (NodeType.TURNOUT_SWITCH, NodeType.TURNOUT_TRACK):
				if turnout is not None:
					self.error("turnout already defined for this switch", s)
					self.note("turnout first defined", self._turnout_location[tod])
					return
				self._turnout_location[tod] = s
				turnout = ("switch" if s.type == NodeType.TURNOUT_SWITCH else "track", *s.items)

		if lever:
			self._switches_to_attach.append((tod, lever, stmt))
		else:
			self.error("lever required for switch", stmt)
			return
		if turnout:
			self._turnouts_to_attach.append((tod, turnout, stmt))

	def _attach_switch(self, tod, lever, stmt):
		_lvr = self._switch_lever_location[tod]
		interlocking, lever, label = self._find_lever(*lever, _lvr)

		try:
			sw = Switch(lever=self._levers[interlocking.id][lever], label=label or "A")
		except SwitchError as e:
			self.error(e.args[0], stmt)
		except LeverError as e:
			self.error(e.args[0], _lvr)
		else:
			self._switches[tod] = sw

			track, offset, direction = tod
			sw.attach(self.network.track(track), offset, direction)

	def _attach_turnout(self, tod, turnout, stmt):
		type, *args = turnout
		if type == "switch":
			_lvr = self._turnout_location[tod]
			interlocking, lever, label = self._find_lever(*args, _lvr)

			lever = self._levers[interlocking.id][lever]
			if label is None and len(lever.controls) > 1:
				self.error("lever label required", self._turnout_location[tod])
				return
			elif label is not None and len(lever.controls) == 1:
				self.warning("lever label '{0}' is redundant", self._turnout_location[tod])
				self._switches[tod].attach_turnout(list(lever.controls.values)[0])
			else:
				self._switches[tod].attach_turnout(lever.controls[label])

		elif type == "track":
			_trk = self._turnout_location[tod]
			track, direction = args

			track = self._tracks[track]
			if track is None:
				self.error("no such track", self._turnout_location[tod])
				return

			try:
				self._switches[tod].attach_turnout(track, direction)
			except SwitchError as e:
				self.error(e.args[0], self._turnout_location[tod])

	def _find_lever(self, interlocking, lever, stmt):
		if interlocking not in self._interlockings:
			self.error("no such interlocking", stmt)
			return

		interlocking = self._interlockings[interlocking]
		label = None
		if lever[-1].isalpha():
			lever, label = lever[:-1], lever[-1].upper()

		lever = int(lever)
		if lever not in self._levers[interlocking.id]:
			self.error("no such lever in interlocking", stmt)
			return

		return (interlocking, lever, label)

	def _handle_station(self, stmt):
		name, block = stmt.items

		if name in self._stations:
			self.error("station already defined", stmt)
			self.note("station first defined", self._station_location[name])

		stn = Station(name, self.network)
		self._stations[name] = stn
		self._station_location[name] = stmt

		for s in block:
			if s.type == NodeType.PLATFORM:
				self._handle_platform(s, stn, stmt)

	def _handle_platform(self, stmt, stn, parent):
		number, length, side = stmt.items
		self._platforms.append((stn, number, length.items, side, stmt, parent))

	def _create_platform(self, station, number, length, side, stmt, parent):
		sections = []
		for section in length:
			track, offset = section.items
			sec = self._find_track_section(track, offset, section)
			if sec:
				sections.append(sec)

		platform = Platform(number, ControlLength([(True, sections)]), side)
		try:
			station.add_platform(platform)
		except StationError as e:
			self.error(e.args[0], stmt)

	def _find_track_section(self, track, offset, stmt):
		if track not in self._tracks:
			self.error("no such track", stmt)
			return
		track = self._tracks[track]
		if offset not in track:
			self.error("offset out of range of track", stmt)
			return
		if offset in track.joints:
			self.error("offset occurs at a joint", stmt)
			return
		return track.section_with_offset(offset)

	def _handle_signal(self, stmt):
		trackoffset, direction, block = stmt.items
		track, offset = trackoffset.items

		tod = (track, offset, direction)

		if tod in self._signals:
			self.error("signal already defined", stmt)
			self.note("signal first defined", self._signal_location[tod])
			return
		self._signals[tod] = None
		self._signal_location[tod] = stmt

		lever = None
		approach = False
		control_length = None
		grade_time = None
		station_time = None

		for s in block:
			if s.type in (NodeType.LEVER_NUMBER, NodeType.APPROACH_LEVER):
				if lever is not None:
					self.error("lever already defined for this signal", s)
					self.note("lever first defined", self._switch_lever_location[tod])
					return
				self._signal_lever_location[tod] = s
				lever = tuple(s.items)
				if s.type == NodeType.APPROACH_LEVER:
					approach = True
			elif s.type == NodeType.CONTROL_LENGTH:
				if control_length is not None:
					self.error("control length already defined for this signal", s)
					self.note("control length first defined", self._control_length_location[tod])
					return
				self._control_length_location[tod] = s
				control_length = s.items[0]
			elif s.type == NodeType.GRADE_TIME:
				if grade_time is not None:
					self.error("grade time already defined for this signal", s)
					self.note("grade time first defined", self._grade_time_location[tod])
					return
				self._grade_time_location[tod] = s
				grade_time = s.items[0]
			elif s.type == NodeType.STATION_TIME:
				if station_time is not None:
					self.error("station time already defined for this signal", s)
					self.note("station time first defined", self._station_time_location[tod])
					return
				self._station_time_location[tod] = s
				station_time = s.items[0]

		att = (*tod, lever, approach, control_length, grade_time, station_time, stmt)
		self._signals_to_attach.append(att)

	def _attach_signal(self, track, offset, direction, lever, approach, \
			control_length, grade_time, station_time, stmt):
		tod = (track, offset, direction)

		interlocking = None
		label = None
		GT = None
		ST = None

		if lever is not None:
			_lvr = self._signal_lever_location[tod]
			interlocking, lever, label = self._find_lever(*lever, _lvr)
			lever = self._levers[interlocking.id][lever]

			if label is None and len(lever.controls) > 1:
				self.error("lever label required", self._turnout_location[tod])
				return
			elif label is not None and len(lever.controls) == 1:
				self.warning("lever label '{0}' is redundant", self._turnout_location[tod])
				lever = list(lever.controls.values())[0]
				label = "A"

			label = label or "A"

		if control_length is not None:
			control_length = self._handle_control_length(control_length, interlocking, stmt)
		else:
			self.warning("control length not defined for signal", stmt)
			control_length = ControlLength([])

		if lever is not None and lever.id == 0:
			lever = None

		if grade_time is not None:
			_GT = self._handle_timing(tod, False, grade_time, interlocking, stmt)
			time, first_time, second_time, timed_section, first_timed_section, second_timed_section = _GT
			GT = Timing(type=TimingType.GRADE, timed_section=timed_section or second_timed_section, \
				time=time or second_time, first_time=first_time, first_timed_section=first_timed_section)
		if station_time is not None:
			_ST = self._handle_timing(tod, True, station_time, interlocking, stmt)
			time, _, __, timed_section, ___, ____ = _ST # _ and __ must be None
			ST = Timing(type=TimingType.STATION, timed_section=timed_section, time=time)

		signal = None
		try:
			signal = Signal(label=label, lever=lever, approach=approach, \
				control_length=control_length, grade_time=GT, station_time=ST)
		except SignalError as e:
			self.error(e.args[0], stmt)
			return

		if not signal:
			return

		self._signals[tod] = signal

		if track not in self._tracks:
			self.error("no such track", stmt)
			return

		try:
			signal.attach(self._tracks[track], offset, direction)
		except SignalError as e:
			self.error(e.args[0], stmt)

	def _handle_control_length(self, control_length, interlocking, stmt):
		if interlocking is None:
			interlocking = self._interlockings["_dummy"]

		conditions = []

		for cond in control_length:
			condition, length = cond.items
			c = self._handle_lever_condition(condition, interlocking, stmt)

			sections = []
			for sec in length.items:
				track, offset = sec.items
				s = self._find_track_section(track, offset, sec)

				if s is not None:
					sections.append(s)

			conditions.append((c, sections))

		return ControlLength(conditions)

	def _handle_timing(self, signal, is_station_time, block, interlocking, stmt):
		time = None
		first_time = None
		second_time = None
		timed_section = None
		first_timed_section = None
		second_timed_section = None

		_time = None
		_first_time = None
		_second_time = None
		_timed_section = None
		_first_timed_section = None
		_second_timed_section = None

		for s in block:
			if s.type == NodeType.TIME:
				if time is not None:
					self.error("time already defined", s)
					self.note("time first defined", _time)
					continue
				if first_time is not None and second_time is not None:
					self.error("times already defined", s)
					self.note("first time defined", _first_time)
					self.note("second time defined", _second_time)
					continue
				time = s.items[0]
				_time = s
			elif s.type == NodeType.FIRST_TIME:
				if is_station_time:
					self.error("cannot have 'first time' for ST signal", s)
					continue
				if time is not None:
					self.error("time already defined", s)
					self.note("time first defined", _time)
					continue
				elif first_time is not None:
					self.error("time already defined", s)
					self.note("time first defined", _first_time)
					continue
				first_time = s.items[0]
				_first_time = s
			elif s.type == NodeType.SECOND_TIME:
				if is_station_time:
					self.error("cannot have 'second time' for ST signal", s)
					continue
				if time is not None:
					self.error("time already defined", s)
					self.note("time first defined", _time)
					continue
				elif second_time is not None:
					self.error("time already defined", s)
					self.note("time first defined", _second_time)
					continue
				second_time = s.items[0]
				_second_time = s
			elif s.type == NodeType.TIMED_SECTION:
				if timed_section is not None:
					self.error("timed section already defined", s)
					self.note("timed section first defined", _timed_section)
					continue
				elif first_timed_section is not None and second_timed_section is not None:
					self.error("timed sections already defined", s)
					self.note("first timed section defined", _first_timed_section)
					self.note("second timed section defined", _second_timed_section)
					continue
				timed_section = self._handle_control_length(s.items[0], interlocking, stmt)
				_timed_section = s
			elif s.type == NodeType.FIRST_TIMED_SECTION:
				if is_station_time:
					self.error("cannot have 'first timed section' for ST signal", s)
					continue
				if first_timed_section is not None:
					self.error("timed section already defined", s)
					self.note("timed section first defined", _first_timed_section)
					continue
				first_timed_section = self._handle_control_length(s.items[0], interlocking, stmt)
				_first_timed_secton = s
			elif s.type == NodeType.SECOND_TIMED_SECTION:
				if is_station_time:
					self.error("cannot have 'second timed section' for ST signal", s)
					continue
				if second_timed_section is not None:
					self.error("timed section already defined", s)
					self.note("timed section first defined", _second_timed_section)
					continue
				second_timed_section = self._handle_control_length(s.items[0], interlocking, stmt)
				_second_timed_section = s

		if time is None and first_time is None and second_time is None:
			self.error("time required for timed signal", stmt)
			return
		if timed_section is None and first_timed_section is None and second_timed_section is None:
			self.error("timing section required for timed signal", stmt)
			return

		return (time, first_time, second_time, timed_section, first_timed_section, second_timed_section)

	def _remove_dummy(self):
		# Remove the _dummy interlocking and lever 0 in each interlocking.
		network = self.network

		for intg in list(network.interlockings):
			if intg.id == "_dummy":
				network.interlockings.remove(intg)
				continue
			for lever in list(intg.levers):
				if lever.id == 0:
					intg.levers.remove(lever)
