from enum import Enum
from s3.direction import Direction
from s3.lever import Lever, LeverState
from s3.track import Track
import asyncio

class SwitchState(Enum):
	"""
	As with a lever, NORMAL and REVERSE are the main states
	and SWITCHING is an intermediate state.
	"""

	NORMAL = 1
	SWITCHING = 2
	REVERSE = 3

class SwitchError(Exception):
	pass

class Switch:
	"""
	A switch in a track layout.
	"""

	# The amount of time to switch between NORMAL and REVERSE.
	# TODO: maybe have this configurable somewhere
	SWITCH_TIME = 2

	def __init__(self, lever=None, label=None):
		if isinstance(lever, Lever):
			self.lever = lever
			lever.network.switches.add(self)
			lever.add(self, label)

			self._state = SwitchState.NORMAL
			self._state_observers = []

			self._locked = False
			self._locked_observers = []

			self.track = None
			self.position = None
			self.direction = None

			# The track (or switch) that this switch turns onto.
			self.turnout = None

			self.network = None
		else:
			raise SwitchError("lever required")

	@property
	def name(self):
		return "{0}{1}".format(self.lever.id, self.label if len(self.lever.controls) > 1 else "")

	def __repr__(self):
		return "<Switch {0}>".format(self.name)

	def __str__(self):
		return "{0}/{1}".format(self.lever.interlocking.id, self.name.lower())

	@property
	def down(self):
		return None

	@property
	def up(self):
		return None

	@property
	def locked(self):
		return self._locked

	@locked.setter
	def locked(self, val):
		v = bool(val)
		if self._locked != v:
			self._locked = v
			for func in self._locked_observers:
				func(self)

	def on_change_locked(self, func):
		self._locked_observers.append(func)

	def on_change_locked_remove(self, func):
		self._locked_observers.remove(func)

	@property
	def state(self):
		return self._state

	@state.setter
	def state(self, state):
		if self._state != state:
			self._state = state
			for func in self._state_observers:
				func(self)

	def on_change_state(self, func):
		self._state_observers.append(func)

	def on_change_state_remove(self, func):
		self._state_observers.remove(func)

	@property
	def normal(self):
		return self.state == SwitchState.NORMAL

	@property
	def reverse(self):
		return self.state == SwitchState.REVERSE

	async def switch(self, state):
		"""
		Switch the switch from NORMAL to REVERSE or vice versa.
		Do nothing if it is currently switching.
		"""

		if self.state == SwitchState.SWITCHING:
			return
		if self.locked:
			return

		if self.state == SwitchState.NORMAL and \
			state == LeverState.REVERSE:
			self.state = SwitchState.SWITCHING
			await asyncio.sleep(Switch.SWITCH_TIME)
			self.state = SwitchState.REVERSE
		elif self.state == SwitchState.REVERSE and \
			state == LeverState.NORMAL:
			self.state = SwitchState.SWITCHING
			await asyncio.sleep(Switch.SWITCH_TIME)
			self.state = SwitchState.NORMAL

	def attach(self, track, position, direction):
		"""
		Attach this switch to the specified track at the specified
		position, such that the diverging track can be used when
		approaching the switch in the specified direction.
		"""
		if not isinstance(track, Track) and not isinstance(position, int) \
			and not isinstance(position, float):
			raise SwitchError("invalid parameters")

		if position not in track:
			raise SwitchError("no such position on track")

		switches = None
		if direction == Direction.UP:
			switches = track.up_switches
		elif direction == Direction.DOWN:
			switches = track.down_switches
		else:
			raise SwitchError("invalid direction")

		switches[position] = self
		self.track = track
		self.position = position
		self.direction = direction

		self.network = track.network
		self.network.switches.add(self)

	def attach_turnout(self, turnout, direction=None):
		"""
		Attach a track or switch to this switch as the turnout track.
		"""
		if self.turnout is not None:
			raise SwitchError("turnout track exists")

		if isinstance(turnout, Switch):
			if turnout.turnout is not None:
				raise SwitchError("turnout track exists")
			self.turnout = turnout
			turnout.turnout = self
		elif isinstance(turnout, Track):
			# If attaching a track, there must be a direction provided,
			# and that direction must not already have any track attached.
			if direction == Direction.UP:
				if turnout.up_next is not None:
					raise SwitchError("track already attached")
				self.turnout = turnout
				turnout.up_next = self
			elif direction == Direction.DOWN:
				if turnout.down_next is not None:
					raise SwitchError("track already attached")
				self.turnout = turnout
				turnout.down_next = self
			else:
				raise SwitchError("valid direction required")
		else:
			raise SwitchError("invalid turnout track")

	@property
	def next_signal(self):
		if isinstance(self.turnout, Track):
			_dir = Direction.UP if self.turnout.down_next == self else Direction.DOWN
			_end = self.turnout.up if _dir == Direction.DOWN else self.turnout.down
			return self.turnout.next_signal(_end, _dir)
		elif isinstance(self.turnout, Switch):
			_dir = Direction.UP if self.turnout.direction == Direction.DOWN else Direction.UP
			return self.turnout.track.next_signal(self.turnout.position, _dir)
