from collections import deque
from .direction import Direction

class TrackNetworkError(Exception):
	pass

class TrackNetwork:
	"""
	The entire network of track to be controlled. Individual
	interlockings may be controlled from here as well.
	"""

	def __init__(self, id):
		self.id = str(id)
		self.interlockings = set()
		self.tracks = set()
		self.signals = set()
		self.switches = set()
		self.stations = set()

	def interlocking(self, id):
		interlockings = [i for i in self.interlockings if i.id == str(id)]
		return interlockings[0] if interlockings != [] else None

	def lever(self, id):
		if "/" not in id:
			return None

		i, l = id.split("/")
		intg = self.interlocking(i)

		try:
			l = int(l)
		except ValueError:
			return None

		return intg.lever(l) if intg is not None else None

	def track(self, id):
		tracks = [t for t in self.tracks if t.id == str(id)]
		return tracks[0] if tracks != [] else None

	def track_section(self, id):
		if "@" not in id:
			return None

		t, o = id.split("@")
		t = self.track(t)

		try:
			o = float(o)
		except ValueError:
			return None

		return t.section_with_offset(o) if t is not None else None

	def lever_object(self, lever):
		lever_, label = lever, "A"
		if lever[-1].islower():
			lever_, label = lever[:-1], lever[-1].upper()
		lvr = self.lever(lever_)
		return lvr.object(label) if lvr is not None else None

	def station(self, name):
		stations = [s for s in self.stations if s.name == str(name)]
		return stations[0] if stations != [] else None

	def routes_from(self, signal):
		return signal.lever.interlocking.routes_from(signal.lever)

	def __repr__(self):
		return "<TrackNetwork {0}>".format(self.id)
