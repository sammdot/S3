from enum import Enum
import asyncio

class LeverState(Enum):
	"""
	Possible states of a lever. NORMAL and REVERSE are the main
	states; SWITCHING is only an intermediate state and levers
	cannot be locked into this state.
	"""

	NORMAL = 1
	SWITCHING = 2
	REVERSE = 3

class LeverError(Exception):
	pass

class LeverStateError(Exception):
	pass

class LeverStateCondition:
	"""
	A Boolean expression that will evaluate to true if the
	lever is in the specified state (NORMAL or REVERSE only).
	"""

	def __init__(self, lever, state):
		if state not in {LeverState.NORMAL, LeverState.REVERSE}:
			raise LeverStateError("invalid lever state: " + str(state))

		self.lever = lever
		self.state = state

	def __repr__(self):
		return "<Lever {0} {1}>".format(self.lever.id, self.state.name)

	def __bool__(self):
		return self.lever.state == self.state

	def __and__(self, cond):
		return And(self, *cond.conditions) if isinstance(cond, And) \
			else And(self, cond)

	def __or__(self, cond):
		return Or(self, *cond.conditions) if isinstance(cond, Or) \
			else Or(self, cond)

class And:
	"""
	Represents a lever state condition where
	all sub-conditions must be satisfied.
	"""

	def __init__(self, *cond):
		self.conditions = cond

	def __repr__(self):
		return "And({0})".format(", ".join(map(str, self.conditions)))

	def __bool__(self):
		return all(self.conditions)

	def __and__(self, cond):
		return And(*(self.conditions + cond.conditions)) if \
			isinstance(cond, And) else And(cond, *self.conditions)

	def __or__(self, cond):
		return Or(self, *cond.conditions) if isinstance(cond, Or) \
			else Or(self, cond)

class Or:
	"""
	Represents a lever state condition where at
	least one sub-condition must be satisfied.
	"""

	def __init__(self, *cond):
		self.conditions = cond

	def __repr__(self):
		return "Or({0})".format(", ".join(map(str, self.conditions)))

	def __bool__(self):
		return any(self.conditions)

	def __and__(self, cond):
		return And(self, *cond.conditions) if isinstance(cond, And) \
			else And(self, cond)

	def __or__(self, cond):
		return Or(*(self.conditions + cond.conditions)) if \
			isinstance(cond, Or) else Or(cond, *self.conditions)

class Lever:
	"""
	An individual lever in an interlocking. Typically controls a
	switch or crossover, or at least one signal, or the direction
	of traffic on a section of track.
	"""

	def __init__(self, id, intg):
		try:
			self.id = int(id)
			if self.id < 0:
				raise LeverError("invalid lever number: " + str(id))
		except ValueError:
			raise LeverError("invalid lever number: " + str(id))

		# Lever numbers must be unique within an interlocking.
		if self.id in {l.id for l in intg.levers}:
			raise LeverError("existing lever in interlocking: " + str(id))

		self.interlocking = intg
		self.network = intg.network
		intg.levers.add(self)

		self._state = LeverState.NORMAL
		self._state_observers = []
		self.controls = {}

		self._locked = False
		self._locked_observers = []

		# The conditions under which this lever can
		# be set to normal or reverse respectively.
		self._normal = None
		self._reverse = None

	def __repr__(self):
		return "<Lever {0}>".format(self.id)

	def __str__(self):
		return "{0}/{1}".format(self.interlocking.id, self.id)

	def object(self, label):
		if len(self.controls) == 1:
			return list(self.controls.values())[0]
		return self.controls.get(label.upper())

	@property
	def state(self):
		return self._state

	@state.setter
	def state(self, state):
		if self._state != state:
			self._state = state
			for func in self._state_observers:
				func(self)

	def on_change_state(self, func):
		self._state_observers.append(func)

	def on_change_state_remove(self, func):
		self._state_observers.remove(func)

	@property
	def locked(self):
		return self._locked

	@locked.setter
	def locked(self, val):
		v = bool(val)
		if self._locked != v:
			self._locked = v
			for func in self._locked_observers:
				func(self)

	def on_change_locked(self, func):
		self._locked_observers.append(func)

	def on_change_locked_remove(self, func):
		self._locked_observers.remove(func)

	@property
	def normal(self):
		"""
		Return a condition of when this lever is set to NORMAL.
		"""

		return LeverStateCondition(self, LeverState.NORMAL)

	@property
	def reverse(self):
		"""
		Return a condition of when this lever is set to REVERSE.
		"""

		return LeverStateCondition(self, LeverState.REVERSE)

	@staticmethod
	def label_is_valid(label):
		return isinstance(label, str) and len(label) == 1 and label.isalpha()

	def add(self, obj, label):
		if label in self.controls:
			raise LeverError("duplicate label exists: " + str(label))
		if Lever.label_is_valid(label):
			self.controls[label.upper()] = obj
			obj.label = label.upper()
		else:
			raise LeverError("invalid label")

	def __contains__(self, obj):
		return self.controls[obj.label] == obj

	def normal_if(self, cond):
		self._normal = cond

	def reverse_if(self, cond):
		self._reverse = cond

	def can_switch(self):
		return ((self.state == LeverState.NORMAL and self._reverse) or \
			(self.state == LeverState.REVERSE and self._normal)) and \
			not self.locked and all(not obj.locked for obj in self.controls.values()) and \
			self.state != LeverState.SWITCHING

	async def switch(self):
		"""
		Switch the lever from NORMAL to REVERSE or vice versa.
		Do nothing if it is currently switching, or if the
		interlocking prevents it.
		"""

		if self.state == LeverState.SWITCHING:
			return

		if self.can_switch():
			new_state = LeverState.REVERSE if self.state == LeverState.NORMAL else LeverState.NORMAL
			self.state = LeverState.SWITCHING

			to_switch = [obj.switch(new_state) for obj in self.controls.values()]
			res = await asyncio.gather(*to_switch)

			self.state = new_state
			return True
		return False
