from enum import Enum
from .control_length import ControlLength
import asyncio

class TimingType(Enum):
	"""
	The two types of timing used in the signal system.
	The actual logic for each type is in the Signal class.
	"""
	GRADE = 1
	STATION = 2

class TimingError(Exception):
	pass

class Timing:
	"""
	Contains information on grade and station times.
	"""

	def __init__(self, type=TimingType.GRADE,
			timed_section=None, first_timed_section=None,
			time=None, first_time=None):
		self.type = type
		self._timed_section = timed_section or ControlLength([])
		self._first_timed_section = first_timed_section or ControlLength([])

		if time is None:
			raise TimingError("time is required")
		self.time = time

		self._active = False

		# This is the time for the first timer
		# for two-shot grade time signals. The
		# second timer is given by self.time.
		self.first_time = None
		if first_time is not None:
			if type == TimingType.GRADE:
				self.first_time = first_time
			else:
				raise TimingError("cannot have two timers for station time")

		self._timer = None
		self._first_timer = None

	@property
	def timed_section(self):
		return self._timed_section.length

	@property
	def first_timed_section(self):
		return self._first_timed_section.length

	@property
	def sections(self):
		return self._timed_section.sections

	@property
	def first_sections(self):
		return self._first_timed_section.sections

	@property
	def occupied(self):
		return self._timed_section.occupied

	@property
	def first_occupied(self):
		return self._first_timed_section.occupied

	@property
	def active(self):
		return self._active

	def __bool__(self):
		return self._active

	def start(self, func):
		def callback():
			self._active = False
			self._timer = None
			func()

		if self._timer is not None:
			self._timer.cancel()

		loop = asyncio.get_event_loop()
		self._timer = loop.call_later(self.time, callback)
		self._active = True

	def start_first(self, func):
		def callback():
			self._active = False
			self._first_timer = None
			func()

		if self.type == TimingType.STATION:
			return
		if self._first_timer is not None:
			self._first_timer.cancel()

		loop = asyncio.get_event_loop()
		self._first_timer = loop.call_later(self.first_time, callback)
		self._active = True

	def cancel(self):
		if self._timer is not None:
			self._timer.cancel()
			self._timer = None
			self._active = False

	def cancel_first(self):
		if self.type == TimingType.STATION:
			return
		if self._first_timer is not None:
			self._first_timer.cancel()
			self._first_timer = None
			self._active = False
