from enum import Enum

class Direction(Enum):
	"""
	A direction on a track or a switch. In railroad parlance,
	only two directions are present, 'north' and 'south', or
	'up' and 'down'.
	"""

	UP = 1
	DOWN = 2
