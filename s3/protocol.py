"""
Classes and methods to be used by clients and
servers implementing the S3 server protocol.
"""

class Message:

	MAX_PARAMS = 16

	def __init__(self, command, *params, prefix=None):
		self.prefix = prefix
		self.command = command

		params = list(params)
		if len(params) >= Message.MAX_PARAMS:
			self.params = params[:Message.MAX_PARAMS - 1] + \
				[" ".join(params[Message.MAX_PARAMS - 1:])]
		else:
			self.params = params[:]

	def __str__(self):
		if self.params:
			*params, lastparam = [str(p) for p in self.params]
			if " " in lastparam:
				lastparam = ":" + lastparam
			params.append(lastparam)
			return "{2}{0} {1}".format(self.command, " ".join(params),
				":{0} ".format(self.prefix) if self.prefix is not None else "")
		else:
			return "{1}{0}".format(self.command,
				":{0} ".format(self.prefix) if self.prefix is not None else "")

	@staticmethod
	def from_line(line):
		if len(line.strip()) == 0:
			return None

		params = []
		prefix, command = None, None

		if line.startswith(":"):
			prefix, command = line.split(" ", 1)
			prefix = prefix[1:]
		else:
			command = line

		command, *trail = command.split(" :", 1)
		command, *params = command.split(" ")
		params.extend(trail)

		return Message(command, *params, prefix=prefix)
