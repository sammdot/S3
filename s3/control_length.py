from collections import OrderedDict

class ControlLength:
	"""
	The control length of a signal is the length of track for which a
	signal will be red if it is occupied (by a train or other object).

	Objects consist of a list of LeverStateConditions and corresponding
	lists of TrackSections; the list of TrackSections for which the
	corresponding LeverStateCondition is satisfied is the control length
	for the signal at the time. (The default case can use 'True'.)
	"""

	def __init__(self, conditions):
		self.conditions = OrderedDict(conditions)

	@property
	def length(self):
		"""
		Return the current control length. If none of the conditions are
		satisfied (or none are defined at all), return an empty list.
		"""
		true_conditions = [cond for cond in self.conditions.keys() if cond]
		if len(true_conditions) == 0:
			return []
		else:
			condition = true_conditions[0]
			return self.conditions[condition]

	@property
	def sections(self):
		"""
		Return the set of sections in this control length.
		"""
		return set(sum([sec for sec in self.conditions.values()], []))

	@property
	def occupied(self):
		"""
		Return whether the current control length is occupied.
		"""
		return any(s.occupied for s in self.length) if self.length else True
