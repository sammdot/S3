from enum import Enum
from .direction import Direction
from .network import TrackNetwork
from .parity import Parity

from operator import attrgetter

class NumberingStyle(Enum):
	NEW_YORK_A = 1
	NEW_YORK_B = 2
	TORONTO = 3

class TrackError(Exception):
	pass

class Track:
	"""
	A section of track, with a single chaining prefix and consistent
	station or chaining numbers in each direction. One end of the
	track is called UP and the other is called DOWN.
	"""

	def __init__(self, trkid, network, style=NumberingStyle.TORONTO, line=None,
		number=None, up_number=None, down_number=None,
		up_prefix=None, down_prefix=None, up=None, down=None,
		up_parity=None, down_parity=None, joints=None):

		self.id = str(trkid)

		# The tracks attached to this track in either direction.
		self.up_next = None
		self.down_next = None

		# The signals attached to this track facing in either direction.
		self.up_signals = {}
		self.down_signals = {}

		# The switches attached to this track facing in either direction.
		self.up_switches = {}
		self.down_switches = {}

		if isinstance(network, TrackNetwork):
			self.network = network
			network.tracks.add(self)
		else:
			raise TrackError("not attached to a track network")

		# The numbering convention used on this section of track.
		# A single track network may use combinations of each convention,
		# so this field is set per track, and not per network.

		if isinstance(style, NumberingStyle):
			self.style = style
		else:
			raise TrackError("invalid 'style' parameter")

		# 'up_prefix' and 'down_prefix' are the chaining prefixes for
		# this track's UP and DOWN directions. The signals on the UP
		# direction will have 'up_prefix' for the prefix, and 'down_prefix'
		# for DOWN.

		# S3 supports three different signal numbering conventions.
		# If signals are numbered in the NYC A- and B- division conventions,
		# the track numbers and chaining lines (see above) are used, and
		# the chaining prefix is automatically generated. If the Toronto
		# convention is used, the chaining prefix should be provided.

		if style == NumberingStyle.TORONTO:
			if up_prefix is None:
				raise TrackError("chaining prefix required")
			if Track.chaining_prefix_is_valid(up_prefix):
				self.up_prefix = up_prefix.upper()
			else:
				raise TrackError("invalid chaining prefix")

			if down_prefix is None:
				raise TrackError("chaining prefix required")
			if Track.chaining_prefix_is_valid(down_prefix):
				self.down_prefix = down_prefix.upper()
			else:
				raise TrackError("invalid chaining prefix")
		else:
			# 'line' is the chaining line prefix, usually used on NYC subway
			# lines. Chaining line prefixes are one or two letters long.
			if line is not None:
				if Track.chaining_line_is_valid(line):
					self.line = line.upper()
				else:
					raise TrackError("invalid chaining line")
			else:
				raise TrackError("chaining line required")

			# 'number' is the track number, usually used on NYC subway lines.
			# Track numbers are one letter or digit long.
			if up_number is not None and down_number is not None:
				if Track.number_is_valid(up_number) and Track.number_is_valid(down_number):
					self.up_number = up_number.upper()
					self.down_number = down_number.upper()
				else:
					raise TrackError("invalid track number")
			elif number is not None:
				if Track.number_is_valid(number):
					self.up_number = self.down_number = number.upper()
				else:
					raise TrackError("invalid track number")
			else:
				raise TrackError("track number required")

			if style == NumberingStyle.NEW_YORK_B:
				self.up_prefix = "{0}{1}".format(line, self.up_number)
				self.down_prefix = "{0}{1}".format(line, self.down_number)
			else:
				self.up_prefix = None
				self.down_prefix = None

		# 'up' and 'down' are the chaining numbers for the UP and DOWN
		# ends of the track respectively. Signal numbers on the track
		# must be between the two values, and must also increase or
		# decrease according to the direction.

		if up is None or down is None:
			raise TrackError("no chaining number")

		if isinstance(up, float) or isinstance(up, int):
			if up < 0:
				raise TrackError("chaining number must be positive")
			self.up = up
		else:
			raise TrackError("invalid chaining number")

		if isinstance(down, float) or isinstance(down, int):
			if down < 0:
				raise TrackError("chaining number must be positive")
			self.down = down
		else:
			raise TrackError("invalid chaining number")

		if up == down:
			raise TrackError("cannot have zero-length track")

		# The direction in which chaining numbers increase.
		self.direction = Direction.UP if up > down else Direction.DOWN

		parities = [Parity.ODD, Parity.EVEN, Parity.ODDEVEN]

		if up_parity is None:
			self.up_parity = Parity.ODDEVEN
		elif up_parity in parities:
			self.up_parity = up_parity
		else:
			raise TrackError("invalid parity: " + str(up_parity))

		if down_parity is None:
			self.down_parity = Parity.ODDEVEN
		elif down_parity in parities:
			self.down_parity = down_parity
		else:
			raise TrackError("invalid parity: " + str(down_parity))

		if self.style == NumberingStyle.NEW_YORK_A:
			# Check for the usual direction of travel as encoded
			# in the `up_parity` and `down_parity` fields.
			up_ = self.up_parity != Parity.ODDEVEN
			down_ = self.down_parity != Parity.ODDEVEN

			if up_ and down_:
				raise TrackError("cannot have both directions on this track")
			elif not up_ and not down_:
				raise TrackError("default direction of travel required on this track")

			self.travel_direction = Direction.UP if up_ else Direction.DOWN

		# Disable parities on NEW_YORK_B tracks.
		elif self.style == NumberingStyle.NEW_YORK_B:
			self.up_parity = Parity.ODDEVEN
			self.down_parity = Parity.ODDEVEN

		# The set of insulated joints on this track. The parameter
		# can be given as any iterable, as long as the values are or
		# can be converted to floats and lie within the track's range.
		self.joints = set()
		for joint in joints or []:
			try:
				jt = float(joint)
				if jt not in self:
					raise TrackError("joint value out of range: " + str(joint))
				self.joints.add(jt)
			except ValueError:
				raise TrackError("invalid joint value: " + str(joint))

		# Create TrackSection objects between every pair of joints
		# in the track, including the ends of the track.
		all_joints = sorted([self.up, self.down] + list(self.joints),
			key=lambda x: x if self.direction == Direction.UP else -x)
		sections = [TrackSection(self, *pair) for pair in zip(all_joints, all_joints[1:])]
		for a, b in zip(sections, sections[1:]):
			a.up_next = b
			b.down_next = a

		self.sections = sections

	def __repr__(self):
		return "<Track {0}>".format(self.id)

	def __str__(self):
		return self.id

	def __contains__(self, num):
		"""
		Return whether the number is within the range of this track,
		i.e. if there is such a position offset on this track.
		"""

		return self.up <= num <= self.down or self.down <= num <= self.up

	@staticmethod
	def chaining_line_is_valid(line):
		return isinstance(line, str) and 1 <= len(line) <= 2 and all(i.isalpha() for i in line)

	@staticmethod
	def number_is_valid(number):
		return isinstance(number, str) and len(number) == 1 and number.isalnum()

	@staticmethod
	def chaining_prefix_is_valid(pfx):
		return isinstance(pfx, str) and all(i.isalnum() for i in pfx)

	def attach(self, track, direction):
		"""
		Attach this track to another track in the specified direction.
		"""

		if direction == Direction.UP:
			if not isinstance(track, Track):
				raise TrackError("invalid track")
			elif self.up_next is not None or \
				track.down_next is not None:
				raise TrackError("track already attached")
			else:
				self.up_next = track
				track.down_next = self
		elif direction == Direction.DOWN:
			if not isinstance(track, Track):
				raise TrackError("invalid track")
			elif self.down_next is not None or \
				track.up_next is not None:
				raise TrackError("track already attached")
			else:
				self.down_next = track
				track.up_next = self
		else:
			raise TrackError("invalid direction")

		# Keep track of sections in adjacent tracks, not just the current one.
		if self.down_next is not None:
			self.sections[0].down_next = self.down_next.sections[-1]
			self.down_next.sections[-1].up_next = self.sections[0]
		if self.up_next is not None:
			self.sections[-1].up_next = self.up_next.sections[0]
			self.up_next.sections[0].down_next = self.sections[-1]

	def section_up_switches(self, section):
		"""
		Return a dictionary of UP-facing switches
		in the specified track section.
		"""

		if section.track != self:
			return {}

		keys = [k for k in self.up_switches.keys() if \
			section.up >= k >= section.down or section.up <= k <= section.down]
		return {k: self.up_switches[k] for k in keys}

	def section_down_switches(self, section):
		"""
		Return a dictionary of DOWN-facing switches
		in the specified track section.
		"""

		if section.track != self:
			return {}

		keys = [k for k in self.down_switches.keys() if \
			section.up >= k >= section.down or section.up <= k <= section.down]
		return {k: self.down_switches[k] for k in keys}

	def section_switches(self, section):
		"""
		Return a list of switches in the specified track section.
		"""
		return list(self.section_up_switches(section).values()) + \
			list(self.section_down_switches(section).values())

	def section_with_offset(self, offset):
		"""
		Return the track section where the specified offset is
		located, or None if it is a joint or out of range.
		"""
		return None if offset not in self or offset in self.joints else \
			[s for s in self.sections if offset in s][0]

	def next_signal(self, offset, direction):
		"""
		Return the first signal that can be reached from the
		given offset when traveling in the given direction.
		"""

		signals = self.up_signals if direction == Direction.UP else self.down_signals
		is_after = (lambda sig: sig.position > offset) if self.direction == direction \
			else (lambda sig: sig.position < offset)
		signals_after = sorted(filter(is_after, signals.values()), \
			key=attrgetter("position"), reverse = (self.direction != direction))

		if signals_after:
			return signals_after[0]
		else:
			from .switch import Switch

			_next = self.up_next if direction == Direction.UP else self.down_next
			if _next is None:
				return None
			elif isinstance(_next, Switch):
				_dir = Direction.UP if _next.direction == Direction.DOWN else Direction.DOWN
				return _next.track.next_signal(_next.position, _dir)
			else:
				_end = _next.down if direction == Direction.UP else _next.up
				return _next.next_signal(_end, direction)

	def no_signals_until_end(self, offset, direction):
		"""
		Return whether there are no more signals past
		the given offset facing in the given direction.
		"""

		signals = self.up_signals if direction == Direction.UP else self.down_signals
		is_after = (lambda sig: sig.position > offset) if self.direction == direction \
			else (lambda sig: sig.position < offset)
		signals_after = sorted(filter(is_after, signals.values()), \
			key=attrgetter("position"), reverse = (self.direction != direction))

		return not signals_after

	def prev_signal(self, offset, direction):
		"""
		Return the first signal that can be reached from the given
		offset when traveling backwards in the given direction.
		"""

		signals = self.up_signals if direction == Direction.UP else self.down_signals
		is_before = (lambda sig: sig.position < offset) if self.direction == direction \
			else (lambda sig: sig.position > offset)
		signals_before = sorted(filter(is_before, signals.values()), \
			key=attrgetter("position"), reverse = (self.direction != direction))

		if signals_before:
			return signals_before[-1]
		else:
			from .switch import Switch

			_next = self.down_next if direction == Direction.UP else self.up_next
			if _next is None:
				return None
			elif isinstance(_next, Switch):
				return _next.track.prev_signal(_next.position, _next.direction)
			else:
				_end = _next.up if direction == Direction.UP else _next.down
				return _next.previous_signal(_end, direction)

	def section_after(self, signal):
		if signal.track != self:
			return None

		return TrackSection(self, signal.position, (self.up if signal.direction == Direction.UP \
			else self.down) if self.no_signals_until_end(signal.position, signal.direction) \
				else signal.next_signal.position)

class TrackSection:
	"""
	A stretch of track between two insulated joints or ends
	of the track. A TrackSection may not contain a joint.
	"""

	def __init__(self, track, up, down):
		if not isinstance(track, Track):
			raise TrackError("invalid track: " + str(track))

		self.track = track

		self.up_next = None
		self.down_next = None

		self._occupied = False
		self._occupied_observers = []
		
		self._locked = False
		self._locked_observers = []

		if up == down:
			raise TrackError("cannot have zero-length section")

		if (track.up > track.down and up < down) \
			or (track.up < track.down and up > down):
			up, down = down, up

		if isinstance(up, float) or isinstance(up, int):
			if up not in track:
				raise TrackError("invalid track offset: " + str(up))
			self.up = up
		else:
			raise TrackError("invalid UP end: " + str(up))

		if isinstance(down, float) or isinstance(down, int):
			if down not in track:
				raise TrackError("invalid track offset: " + str(down))
			self.down = down
		else:
			raise TrackError("invalid DOWN end: " + str(down))

	def __repr__(self):
		return "<Track {0} from {1} to {2}>".format(self.track.id, self.up, self.down)

	def __str__(self):
		return "{0}@{1}".format(self.track.id, (self.up + self.down) / 2)

	def __contains__(self, position):
		"""
		Return whether a certain offset on a track is within
		the range of this track section.
		"""

		return self.up >= position >= self.down or self.up <= position <= self.down

	@property
	def occupied(self):
		return self._occupied

	@occupied.setter
	def occupied(self, val):
		v = bool(val)
		if self._occupied != v:
			self._occupied = v
			if not v and self.locked:
				self.locked = False
			for switch in self.track.section_switches(self):
				switch.locked = v
				# Includes case where the track is occupied
				# and switches should not be thrown.
			for func in self._occupied_observers:
				func(self)

	def on_change_occupied(self, func):
		self._occupied_observers.append(func)

	def on_change_occupied_remove(self, func):
		self._occupied_observers.remove(func)

	@property
	def locked(self):
		return self._locked

	@locked.setter
	def locked(self, val):
		v = bool(val)
		if self._locked != v:
			self._locked = v
			for func in self._locked_observers:
				func(self)

	def on_change_locked(self, func):
		self._locked_observers.append(func)

	def on_change_locked_remove(self, func):
		self._locked_observers.remove(func)

	@property
	def up_switches(self):
		if self.track is None:
			return {}
		return self.track.section_up_switches(self)

	@property
	def down_switches(self):
		if self.track is None:
			return {}
		return self.track.section_down_switches(self)

	@property
	def switches(self):
		if self.track is None:
			return []
		return self.track.section_switches(self)
